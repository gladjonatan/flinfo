<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Simple hook manager. Hooks are global or static functions, once registered by name,
 * they can be called through register. Multiple functions can be registered for a hook;
 * they will be called in the sequence they were registered.
 * 
 * A hook is supposed to return one of three things:
 * - True if processing should be continued
 * - False if processing should be stopped
 * - A String containing an error message if something went wrong.
 * 
 * Hook processing continues while hook functions return true and is stopped otherwise.
 * The last hook function's return value will be returned from FlinfoHooks::run.
 */
abstract class FlinfoHooks {
	
	private static $hooks = array ();
	
	/**
	 * Register a function by name as a hook
	 * 
	 * @param String $hook The hook to register the function for
	 * @param String $functionName The name of the function ("globalFunc" or "SomeClass::staticFunc")
	 * @return void
	 */
	public static function register ($hook, $functionName) {
		if (!isset (self::$hooks[$hook])) {
			self::$hooks[$hook] = array ();
		}
		self::$hooks[$hook][] = $functionName;
	}
	
	/**
	 * Run the registered function for the given hook until one of them does not return true.
	 * 
	 * @param String $hook Id of the Hook
	 * @param array $args (Optional) Arguments for the hook functions
	 * @return unknown_type (Boolean or String) Return value of the last function.
	 */
	public static function run ($hook, $args = array()) {
		if (!isset(self::$hooks[$hook])) {
			return true;
		}
		foreach (self::$hooks[$hook] as $func) {
			if (!is_callable ($func)) continue;
		    $result = call_user_func_array( $func, $args );
		    // Return values: true to continue, false to stop, String error message.
		    if ($result === null) {
		    	return "Hook function $func for hook $hook failed to return a value.";
		    } else if ($result === false) {
		    	return false;
		    } else if (is_string ($result)) {
		    	return $result;
		    }
		}
		return true;
	}
}