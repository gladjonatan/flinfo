<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2011 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Flinfo input handler for www.mushroomobserver.org
 */
require_once ('FlinfoData.php');
require_once ('FlinfoHooks.php');
require_once ('FlinfoStatus.php');
require_once ('FlinfoIn.php');

require_once ('lib/Curly.php');

/**
 * Input handler for www.mushroomobserver.org. Uses screenscraping.
 */
class FlinfoMushroom extends FlinfoIn {

	const MUSHROOM_USERS = "mushroom_users.txt";

	private $mRawId = null;
	private $mStatus = null;
	private $mId = null;
	private $mSizes = null;
	private $mUserId = null;
	private $mUserName = null;
	private $mUserUrl = null;
	private $mTitle = null;
	private $mDescription = null;
	private $mLicense = null;
	private $mLicenseUrl = null;
	private $mGeo = null;
	private $mRawDate = null;
	private $mDate = 0;
	private $mDescUrl = null;
	private $mObservation = null;
	private $mGenus = null;
	private $mSpecies = null;
	private $mAuthority = null;
	private $mWarnMissingSpecies = null;
	private $mLocation = null;
	private $mLocationId = null;
	private $mOverrides = null;

	public function __construct ($parameterFileName, $requestParams) {
		FlinfoHooks::register ('flinfoHtmlAfterTextArea', array ($this, 'htmlHook'));
	}

	private function extractId ($rawId) {
	    if (preg_match ('/^\d+$/', $rawId)) {
			// All digits: assume a photo id.
			return $rawId;
	    } else if (preg_match ('!^https?://(www\.)?mushroomobserver\.org/image/show_image/(\d+)(\?|$)!', $rawId, $matches)) {
	    	return $matches[2];
	    } else if (preg_match ('!^https?://(www\.)?mushroomobserver\.org/image/show_image\?.*?\bid=(\d+)(\&|$)!', $rawId, $matches)) {
	    	return $matches[2];
	    } else if (preg_match ('!^https?://images\.digitalmycology\.com/[^/]+/(\d+)\.[Jj][Pp][Ee]?[Gg]$!', $rawId, $matches)) {
	    	return $matches[1];
		} else {
			// Id could not be determined
			return null;
		}
	}

	public function getInfo ($id) {
		$this->mRawId = $id;
		$this->mId = $this->extractId ($id);
		if (!$this->mId) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($id, $this->mStatus);
		}
		$this->mDescUrl = "http://mushroomobserver.org/image/show_image?_js=off&_new=true&id=" . $this->mId;
		$info = Curly::getContents ($this->mDescUrl);
		if (!$info) {
			$this->mStatus = FlinfoStatus::STATUS_SERVER_FAILURE;
			return array ($this->mId, $this->mStatus);
		}
		// Sanity check
		if (!preg_match ('!<a\s+href="/image/show_image(?:/|\?id=)' . preg_quote ($this->mId, '!') . '[^"]*"!', $info)) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($this->mId, $this->mStatus);
		}
		// Extract the info from the HTML (Screenscraping)
		if (preg_match ('!<div\s+id="copy[rt]ight">((\s|\S)*?)</div>!', $info, $matches)) {
			$copyrightText = trim ($matches[1]);
			if (preg_match ('!<a\s+href="/observer/show_user(?:/|\?id=)(\d+)">([^<]*)</a>!', $copyrightText, $matches)) {
				$this->mUserId = $matches[1];
				$this->mUserName = $matches[2];
				$this->mUserUrl = "http://mushroomobserver.org/observer/show_user/" . $this->mUserId;
			} else {
				$this->mUserId = null;
				$this->mUserName = $copyrightText;
				$this->mUserUrl = null;
			}
		}
		if (preg_match ('!<title>\s*([^<]+)\s*</title>!', $info, $matches)) {
			$this->mTitle = html_entity_decode($matches[1]); // Leading and trailing blanks are already stripped off
			// Split in words. First is the genus, then the species, then the autor, followed by the image id.
			if (preg_match ('!^Mushroom Observer:\s+\S+?:\s+(\S+)\s+(\S+)\s+(.*?)\(' . preg_quote ($this->mId, '!') . '\)!', $this->mTitle, $matches)) {
				$this->mGenus = $matches[1];
				$this->mSpecies = $matches[2];
				$this->mAuthority = trim ($matches[3]);
				if (strlen ($this->mAuthority) == 0) $this->mAuthority = null;
			}
		}
		// Find the thumbnail and create a full_size link.
		$this->mSizes = array ();
		$minW = null;
		$minH = null;
		if (preg_match ('!<a\s+href="/image/show_image(?:/|\?id=)' . preg_quote ($this->mId, '!') . '(?:\?|\S*&(?:amp;)?)size=thumbnail"\s+title="(\d+)\s+\S*\s+(\d+)[^"]*"\s*>!', $info, $matches)) {
			$minW = $matches[1] + 0;
			$minH = $matches[2] + 0;
			$this->mSizes[] = array ("width" => $minW, "height" => $minH, "source" => 'http://images.digitalmycology.com/thumb/' . $this->mId . '.jpg');
		}
		// Find the size of the largest available version
		$sizes = array ('small', 'medium', 'large', 'huge', 'full_size');
		$maxW = null;
		$maxH = null;
		foreach ($sizes as $size) {
			if (preg_match ('!<a\s+href="/image/show_image(?:/|\?id=)' . preg_quote ($this->mId, '!') . '(?:\?|\S*&(?:amp;)?)size=' . $size . '"\s+title="(\d+)\s+\S*\s+(\d+)[^"]*"\s*>!', $info, $matches)) {
				$maxW = $matches[1] + 0;
				$maxH = $matches[2] + 0;
			}
		}
		if (!$maxW) {
			if (preg_match ('!<img\s+.*?src="/images/(\d+)/' . preg_quote ($this->mId, '!') . '\.jpg"!', $info, $matches)) {
				$maxW = $matches[1] + 0;
				$maxH = ($minW ? ($maxW * $minH) / $minW : $maxW);
			}
		}
		$this->mSizes[] = array ("width" => $maxW, "height" => $maxH, "source" => 'http://images.digitalmycology.com/orig/' . $this->mId . '.jpg');
		if (preg_match ('!<a\s+rel="license"\s+href="([^"]*)"\s*>!', $info, $matches)) {
			$this->mLicenseUrl = $matches[1];
			$this->mLicense = self::ccLicenseFromUrl ($this->mLicenseUrl, $this->mUserName);
		}
		// Date: is not marked specially...
		if (preg_match ('!<span\s+class="Data"\s*>((\d\d\d\d)-(\d\d)-(\d\d))</span>!', $info, $matches)) {
			$this->mRawDate = $matches[1];
			$this->mDate = mktime(0, 0, 0, $matches[3], $matches[4], $matches[2]);
		}
		// Location : find the observation link. Don't do this if the license failed already.
		if ($this->mLicense) {
			if (preg_match ('!<a\s+href="/observer/show_observation/(\d+)(\?[^"]*)?"!', $info, $matches)) {
				$this->mObservation = $matches[1];
				$observation = Curly::getContents ('http://mushroomobserver.org/observer/show_observation/' . $matches[1] . '?_js=off&_new=true&id=' . $matches[1]);
			} else if (preg_match ('!<a\s+href="/obs/(\d+)(\?[^"]*)?"!', $info, $matches)) {
				$this->mObservation = $matches[1];
				$observation = Curly::getContents ('http://mushroomobserver.org/obs/' . $matches[1] . '?_js=off&_new=true&id=' . $matches[1]);
			} else if (preg_match ('!<a\s+href="/(\d+)(?:\?[^"]*)?"[^>]*>.*?\(\1\)\s*</a\s*>!', $info, $matches)) {
				$this->mObservation = $matches[1];
				$observation = Curly::getContents ('http://mushroomobserver.org/' . $matches[1] . '?_js=off&_new=true&id=' . $matches[1]);
			} else {
				$observation = null;
			}
			if ($observation) {
				// Sanity check: do we have a link back to the image?
				if (preg_match ('!<a\s+href="/image/show_image(?:/|\?id=)' . preg_quote ($this->mId, '!') . '(?:(?:\?|\S*&(?:amp;)?)[^"]*)?"!', $observation, $matches)) {
					// User still undefined: try again.
					if (!$this->mUserId && preg_match ('!<a\s+href="/observer/show_user(?:/|\?id=)(\d+)"\s*>([^<]*)</a>!', $observation, $matches)) {
						$this->mUserName = trim ($matches[2]);
						$this->mUserId = $matches[1];
						$this->mUserUrl = "http://mushroomobserver.org/observer/show_user/" . $this->mUserId;
					}
				} else {
					$observation = null;
				}
			}
			if ($observation) {
				// Double-check the date; sometimes observations have earlier dates?!
				if (preg_match ('!<span\s+class="Data"\s*>((\d\d\d\d)-(\d\d)-(\d\d))</span>!', $observation, $matches)) {
					$rawDate = $matches[1];
					if (!$this->mRawDate || strcmp ($rawDate, $this->mRawDate) < 0) {
						$this->mRawDate = $rawDate;
						$this->mDate = mktime(0, 0, 0, $matches[3], $matches[4], $matches[2]);
					}
				}
				// Geolocation
				if (preg_match ('!<a\s+href="/location/show_location(?:/|\?id=)(\d+)"\s*>(.*?)</a>!s', $observation, $matches)) {
					$this->mLocationId = $matches[1];
					$locationName = trim(html_entity_decode(strip_tags($matches[2])));
					$pos = strrpos ($locationName, '[');
					if ($pos !== false) {
						$locationName = trim(substr($locationName, 0, $pos));
					}
					$this->mLocation = $locationName;
					// All right; let's try to find the coordinates
					if (!$this->checkAndSetGeo($observation)) {
						$this->checkAndSetGeo(Curly::getContents ('http://mushroomobserver.org/location/show_location/' . $matches[1]));
					}
				} else if (preg_match ('!<a\s+href="/observer/observations_at_where\?[^"]*"\s*>(.*?)</a>!s', $observation, $matches)) {
					$locationName = trim(strip_tags(html_entity_decode($matches[1])));
					$pos = strrpos ($locationName, '[');
					if ($pos !== false) {
						$locationName = trim(substr($locationName, 0, $pos));
					}
					$this->mLocation = $locationName;
				}
				// Try to find a description
				$description = null;
				if (preg_match ('!<div\s+class="textile"[^>]*>(.*?)</div!s', $observation, $matches)) {
					$description = $matches[1];
					$description =
						str_replace (
							array ('<p>', '</p>', '<br />', '<br/>')
						   ,array ("", "\n", ' ', ' ')
						   ,$description
						);
					$description = trim($description);
					if (preg_match ('!^Notes:!', $description)) {
						$description = trim(substr ($description, 6));
					}
				}
				if (preg_match ('!<img.*?src="/(?:images|assets)/eyes3.png".*?/>\s*</td\s*>\s*</tr\s*>\s*<tr[^>]*>\s*<td[^>]*>(.*?)</td!s', $observation, $matches)) {
					$lines = explode("\n", $matches[1]);
					$newLines = array();
					foreach ($lines as $line) {
						$l = trim ($line);
						$l = str_replace (array ('<p>', '</p>'), array ("", "\n"), $l);
						$l = str_replace (array ('<br />', '<br/>'), array ("\n", "\n"), $l);
						if (strlen($l) > 0) $newLines[] = trim($l);
					}
					$lines = implode ("\n", $newLines);
					if ($description) {
						$description .= "\n\n" . $lines;
					} else {
						$description = $lines;
					}
				}
				if (preg_match('!<div\s+class="thumbnail"[^>]*>\s*<a\s+href="/image/show_image(?:/|\?id=)' . preg_quote ($this->mId, '!') . '(?:(?:\?|\S*&(?:amp;)?)[^"]*)?"[^>]*>.*?</div>\s*<small[^>]*>\s*<span\s+title="([^"]+)"!', $observation, $matches)) {
					if ($description) {
						$description .= "\n" . $matches[1];
					} else {
						$description = $matches[1];
					}
				}
				$this->mDescription = $description;
			}
		}
		$this->mStatus = FlinfoStatus::STATUS_OK;
		return array ($this->mId, $this->mStatus);
	}

	private function checkAndSetGeo($input) {
		if ($input) {
			if (preg_match ('!addInfoWindowToMarker\s*\(\s*new\s+GMarker\s*\(\s*new\s+GLatLng\s*\(\s*([^,]+),([^),]+)\s*\)!', $input, $matches)) {
				$lat = trim ($matches[1]);
				$lon = trim ($matches[2]);
				$this->mGeo = array ("latitude" => $lat, "longitude" => $lon, "source" => "mushroom_observer");
				return true;
			} else if (preg_match('!P\s*?\(\s*?M\s*?,\s*?(\d+(?:\.\d*))\s*?,\s*?(\d+(?:\.\d*))\s*?,!', $input, $matches)) {
				$lat = $matches[1];
				$lon = $matches[2];
				$this->mGeo = array ("latitude" => $lat, "longitude" => $lon, "source" => "mushroom_observer");
				return true;
			}
		}
		return false;
	}

	public function getAccountId () {
		return $this->mUserId;
	}

	public function getAuthor () {
		return array (array ($this->mUserUrl, $this->mUserName, null));
	}

	public function getWikiAuthor () {
		return '{{MushroomObserverUser|1=' . FlinfoData::replaceBrackets($this->mUserId)
			. '|2=' . FlinfoData::replaceBrackets($this->mUserName) . '}}';
	}

	public function getSource () {
		return array ('http://mushroomobserver.org/image/show_image/' . $this->mId, $this->getTitle());
	}

	public function getAlternateSource() {
		return 'http://www.mushroomobserver.org/image/show_image/' . $this->mId;
	}

	public function getWikiSource () {
		return '{{MushroomObserver|' . FlinfoData::replaceBrackets($this->mId) . '}}';
	}

	private function getSpecialLicense ($userId) {
		if (!$userId) return null;
		if (!$this->mOverrides) {
			$this->mOverrides = $this->loadParamFile (self::MUSHROOM_USERS);
		}
		if (isset ($this->mOverrides[$userId])) {
			return $this->mOverrides[$userId];
		}
		return null;
	}

	public function getLicenses ($goodUser) {
		$tags = array ();
		$status = $this->mLicenseUrl ? $this->mLicenseUrl : 'Unknown License';
		if ($goodUser) {
			if (!$this->mLicense || preg_match('!^pd-author!', $this->mLicense)) {
				// Check the name file
				$license = $this->getSpecialLicense($this->mUserId);
				if ($license) {
					$tags[] = $license;
					$status = null;
					$tags[] = $this->getReviewTag();
				}
			} else {
				$tags[] = $this->mLicense;
				$status = null;
				$tags[] = $this->getReviewTag();
			}
		}
		return array ($status, $tags, null);
	}

	protected function internalGetReviewTag () {
		return "licensereview";
	}

	public function getTitle () {
		if ($this->mGenus) {
			return $this->mGenus . ' ' . $this->mSpecies . ($this->mAuthority ? ' ' . $this->mAuthority : "");
		}
		return $this->mTitle;
	}

    public function getUploadTitle () {
		if ($this->mRawDate && $this->mGenus) {
			$title = trim ($this->mRawDate . ' ' . $this->getTitle());
			// Avoid ..jpg at the end:
			$length = strlen ($title);
			if (strrpos ($title, '.') === $length - 1) {
				$title = substr ($title, 0, $length - 1);
				$length--;
			}
			if ($length == 0) return null;
			return $title . ' ' . $this->mId;
		}
		return null;
    }

	public function getDescription () {
		$desc = null;
		if ($this->mGenus) {
			$desc = '<b><i>' . $this->mGenus . ' ' . $this->mSpecies . '</i></b>' . ($this->mAuthority ? ' ' . $this->mAuthority : "");
		} else {
			$desc = $this->mTitle;
		}
		if ($this->mLocation) {
			$desc .= "\n\nImage location: " . $this->mLocation;
		}
		if ($this->mDescription) {
			$desc .= "\n\n" . $this->mDescription;
		}
		return $desc;
	}

	public function getWikiDescription () {
		if ($this->mObservation) {
			return '{{MushroomObserverMoreInfo|' . $this->mObservation . '}}';
		}
		return null;
	}

	public function getCategories () {
		$tags = array ();
		if ($this->mGenus) $tags[] = $this->mGenus;
		if ($this->mSpecies) $tags[] = $this->mGenus . ' ' . $this->mSpecies;
		if ($this->mLocation) {
			// Try to build a few sensible locations
			$tmp = explode (',', $this->mLocation);
			foreach ($tmp as $loc) {
				$tags[] = trim ($loc);
			}
			$tmp = explode (';', $this->mLocation);
			foreach ($tmp as $loc) {
				$tags[] = trim ($loc);
			}
		}
		return $tags;
	}

	public function filterCategories ($categories) {
		// Remove any categories that are prefixes of other categories. (Happens often with "Genus" and "Genus species".)
		if (!$categories) {
			if ($this->mGenus) {
				if ($this->mSpecies) {
					$this->mWarnMissingSpecies = $this->mGenus . ' ' . $this->mSpecies;
				} else {
					$this->mWarnMissingSpecies = $this->mGenus;
				}
			}
			return array();
		}
		$map = array_fill_keys ($categories, true);
		sort($categories);
		$length = count ($categories);
		for ($i = 0; $i + 1 < $length; $i++) {
			if (strpos ($categories[$i+1], $categories[$i]) === 0) {
				unset($map[$categories[$i]]);
			}
		}
		// Check whether Genus and/or species are recognized as categories. If not, we'll use the htmlHook to generate a warning later on.
		if ($this->mGenus) {
			$genus = 'Category:' . $this->mGenus;
			$species = null;
			if ($this->mSpecies != 'sp.' && $this->mSpecies != 'spp.') {
				$species = $genus . ' ' . $this->mSpecies;
			}
			if ($species) {
				if (!isset($map[$species])) {
					$this->mWarnMissingSpecies = $species;
				}
			} else if (!isset($map[$genus])) {
				$this->mWarnMissingSpecies = $genus;
			}
			if ($this->mWarnMissingSpecies) {
				$this->mWarnMissingSpecies = substr ($this->mWarnMissingSpecies, 9);
			}
		}
		return array_keys($map);
	}

	public function htmlHook (&$htmlText) {
		if ($this->mWarnMissingSpecies) {
			$html = FlinfoHtmlGen::openElement ('div', array ('style' => 'background:pink;'));
			$html .= FlinfoHtmlGen::text(str_replace ('_SPECIES_', $this->mWarnMissingSpecies, FlinfoData::msg('missing_species')));
			$html .= FlinfoHtmlGen::close ('div');
			$htmlText .= $html;
		}
		return true;
	}

	public function getDate () {
		return $this->mDate;
	}

	public function getDateFormat () {
		return "%Y-%m-%d";
	}

	public function getGeoInfo () {
		return $this->mGeo;
	}

	public function getSizes () {
		return $this->mSizes;
	}

	/**
	 * Fake a JSON server result and return that.
	 *
	 * @return Faked "raw" server result.
	 */
	public function getRawResult () {
		if ($this->mStatus === null) return null;
		$result = array ("status" => $this->mStatus, "raw_id" => $this->mRawId);
		$photo = null;
		if ($this->mId !== null) {
			$photo = array ();
			$photo["id"] = $this->mId;
		}
		if ($this->mStatus == FlinfoStatus::STATUS_OK) {
			$photo["url"] = $this->mDescUrl;
			$photo["title"] = $this->mTitle;
			$photo["owner"] = array ("userId" => $this->mUserId, "userName" => $this->mUserName, "userPage" => $this->mUserUrl);
			$photo["license"] = array ("key" => $this->mLicense, "link" => $this->mLicenseUrl);
			if ($this->mDate) $photo["date"] = $this->mRawDate;
			$photo["sizes"] = $this->mSizes;
			if ($this->mGeo) $photo["geo"] = $this->mGeo;
		}
		if ($photo) $result["photo"] = $photo;
		return $result;
	}

}