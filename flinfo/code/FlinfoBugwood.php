<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2011 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Flinfo input handler for images.bugwwod.org sites:
 * www.insectimages.org
 * www.forestryimages.org
 * www.invasive.org
 * www.weedimages.org
 * www.ipmimages.org
 */
require_once ('FlinfoData.php');
require_once ('FlinfoHooks.php');
require_once ('FlinfoStatus.php');
require_once ('FlinfoIn.php');

require_once ('lib/Curly.php');

/**
 * Input handler for bugwood sites. Uses screenscraping.
 */
class FlinfoBugwood extends FlinfoIn {

	const BUGWOOD_ORGANIZATIONS = "bugwood_organizations.txt";

	private $mRawId = null;
	private $mStatus = null;
	private $mId = null;
	private $mSizes = null;
	private $mUserId = null;
	private $mUserName = null;
	private $mUserUrl = null;
	private $mUserLocation = null;
	private $mOrgId = null;
	private $mOrgName = null;
	private $mOrgUrl = null;
	private $mLicense = null;
	private $mOriginalLicense = null;
	private $mLicenseUrl = null;
	private $mGeo = null;
	private $mRawDate = null;
	private $mDate = 0;
	private $mDescriptor = null;
	private $mDescription = null;
	private $mObservation = null;
	private $mCommonName = null;
	private $mGenus = null;
	private $mSpecies = null;
	private $mAuthority = null;
	private $mWarnMissingSpecies = null;
	private $mLocation = null;
	private $mSite = null;
	private $mSiteName = null;
	private $mNiceSiteName = null;
	private $mSiteTemplate = "";

	public function __construct ($parameterFileName, $requestParams) {
		$text = $this->getParamFile($parameterFileName);
		if ($text) {
			$lines = explode ("\n", $text);
			if (count ($lines) > 1) {
				$this->mSiteName = trim($lines[0]);
				$this->mNiceSiteName = trim($lines[1]);
				$this->mSite = 'http://www.' . $this->mSiteName . '.org';
				FlinfoHooks::register ('flinfoHtmlAfterTextArea', array ($this, 'htmlHook'));
				if (count ($lines) > 2) {
					$this->mSiteTemplate = trim($lines[2]);
				}
			}
		}
	}

	private function extractId ($rawId) {
	    if (preg_match ('/^\d+$/', $rawId)) {
			// All digits: assume a photo id.
			return $rawId;
	    }
	    if (preg_match ('!^https?://(www\.)?' . $this->mSiteName . '\.org/browse/detail.cfm\?imgnum=(\d+)(\&|$)!', $rawId, $matches)) {
	    	return $matches[2];
	    }
	    if (preg_match ('!^https?://(www\.)?(' . $this->mSiteName . '|bugwood)\.org/images//?[^/]+/(\d+).jpg!', $rawId, $matches)) {
	    	return $matches[3];
	    }
	    if (preg_match ('!^https?://(www\.)?' . $this->mSiteName . '\.org/browse/imgdownjoe.cfm\?img=(\d+)(\&|$)!', $rawId, $matches)) {
	    	return $matches[2];
		}
		// Id could not be determined
		return null;
	}

	private function getField ($field, $htmlText) {
		if (preg_match ('!<strong\s*>\s*' . $field . '\s*</strong>((\s|\S)*?)<br!', $htmlText, $matches)) {
			return trim($matches[1]);
		}
		return null;
	}

	public function getInfo ($id) {
		if (!$this->mSite) {
			$this->mStatus = FlinfoStatus::STATUS_INTERNAL_ERROR;
			return array ($id, $this->mStatus);
		}
		$this->mRawId = $id;
		$this->mId = $this->extractId ($id);
		if (!$this->mId) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($id, $this->mStatus);
		}
		$this->mDescUrl = $this->mSite . '/browse/detail.cfm?imgnum=' . $this->mId;
		$info = Curly::getContents ($this->mDescUrl);
		if (!$info) {
			$this->mStatus = FlinfoStatus::STATUS_SERVER_FAILURE;
			return array ($this->mId, $this->mStatus);
		}
		// Sanity check:
		if (!preg_match ('!<link\s+rel="image_src"\s+href=".*?' . preg_quote ($this->mId, '!') . '.jpg"\s+/>!', $info)) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($this->mId, $this->mStatus);
		}
		// Extract the info from the HTML (Screenscraping)
		// Author
		if (preg_match ('!<a\s+href="(/browse/)?autimages.cfm\?aut=(\d+)"\s*>((\s|\S)*?)</a>!', $info, $matches)) {
			$this->mUserId = $matches[2];
			$this->mUserName = trim(html_entity_decode(strip_tags($matches[3])));
			$this->mUserUrl = $this->mSite . '/browse/autimages.cfm?aut=' . $this->mUserId;
		}
		// Organization
		if (preg_match ('!<a\s+href="(/browse/)?orgimages.cfm\?org=([^"]+)"\s*>((\s|\S)*?)</a>!', $info, $matches)) {
			$this->mOrgId = $matches[2];
			$this->mOrgName = trim(html_entity_decode(strip_tags($matches[3])));
			$this->mOrgUrl = $this->mSite . '/browse/orgimages.cfm?org=' . $this->mOrgId;
		} else if (preg_match ('!<a\s+href="(/stats/)?statsorg.cfm\?org=([^"]+)"\s*>((\s|\S)*?)</a>!', $info, $matches)) {
			$this->mOrgId = $matches[2];
			$this->mOrgName = trim(html_entity_decode(strip_tags($matches[3])));
			$this->mOrgUrl = $this->mSite . '/stats/statsorg.cfm?org=' . $this->mOrgId;
		}
		// Genus, species, common name, Authoriy <a href="subthumb.cfm?sub=10521">
		if (preg_match ('!<a\s+href="(/browse/)?subthumb.cfm\?sub=(\d+)"\s*>((\s|\S)*?)</a>!', $info, $matches)) {
			$this->mSpeciesId = $matches[2];
			$htmlText = trim($matches[3]);
			// Three <strong></strong> pairs: common name, genus, species; followed by a span for the authority
			$i = 0; $length = strlen($htmlText); $n = 0;
			while ($i < $length && $n < 3) {
				$pos = strpos ($htmlText, '<strong>', $i);
				if ($pos === false) break;
				$from = $pos + 8;
				$pos = strpos ($htmlText, '</strong>', $pos);
				$to = $pos === false ? $length : $pos;
				$i = $to + 9;
				$text = trim(html_entity_decode(strip_tags(substr ($htmlText, $from, $to - $from))));
				if ($text && strlen($text) > 0) {
					if ($n == 0) $this->mCommonName = $text;
					else if ($n == 1) $this->mGenus = $text;
					else if ($n == 2) $this->mSpecies = $text;
				}
				$n++;
			}
			if ($i < $length) {
				$htmlText = substr ($htmlText, $i);
				if (preg_match ('!<span[^>]*>((\s|\S)*?)</span>!', $htmlText, $matches)) {
					$this->mAuthority = trim(html_entity_decode(strip_tags($matches[1])));
				}
			}
		}
		if (preg_match ('!<a\s+rel="license"\s+href="([^"]*)"[^>]*>!', $info, $matches)) {
			$this->mLicenseUrl = html_entity_decode($matches[1]);
			$this->mLicense = self::ccLicenseFromUrl ($this->mLicenseUrl, $this->mUserName);
		}
		// Sizes
		$this->mSizes = array();
		if (preg_match ('!<div\s+class="detail"\s*>\s*<ul\s[^>]*>\s*<li\s*>\s*<a\s+href="(/browse/)?imgdownjoe.cfm(\s|\S)+?</div>!', $info, $matches)) {
			$htmlText = $matches[0];
			$matches = array();
			if (preg_match_all ('!<li\s*>\s*<a\s+href="([^"]*)"[^>]*>((\s|\S)*?)</a>!', $htmlText, $matches, PREG_SET_ORDER)) {
				$nofMatches = count($matches);
				$width = null;
				$height = null;
				$idx = null;
				for ($i = 0; $i < $nofMatches; $i++) {
					$href = html_entity_decode($matches[$i][1]);
					$urlPrefix = trim(html_entity_decode(strip_tags($matches[$i][2])));
					// urlPrefix is WidthxHeight
					if (preg_match ('!(\d+)\D+(\d+)!', $urlPrefix, $numbers)) {
						$width = $numbers[1];
						$height = $numbers[2];
						$idx = $i + 1;
						// Construct view links (as opposed to D/L links)
						$this->mSizes[] = array (
							'width' => $width
						   ,'height' => $height
						   ,'source' => 'http://www.' . $this->mSiteName . '.org/images/' . $width . 'x' . $height . '/' . $this->mId . '.jpg'
						);
						// http://www.insectimages.org/images/$urlPrefix/$mId.jpg
					}
				}
			}
		}
		$matches = array ();
		// Date: is not marked specially...
		if (preg_match ('!Image Information last updated on([^<]*)!', $info, $matches)) {
			$this->mRawDate = trim (html_entity_decode($matches[1]));
			$this->mDate = strtotime ($this->mRawDate);
		}
		$this->mDescriptor = $this->getField('Descriptor:', $info);
		$this->mDescription = $this->getField('Description:', $info);
		$this->mLocation = $this->getField('Image location:', $info);
		$this->mUserLocation = $this->getField('Country:', $info);

		$this->mStatus = FlinfoStatus::STATUS_OK;
		return array ($this->mId, $this->mStatus);
	}

	public function getAccountId () {
		return $this->mUserId;
	}

	public function getAuthor () {
		$result = array();
		$result[] = array ($this->mUserUrl, $this->mUserName, $this->mUserLocation);
		if ($this->mOrgUrl && $this->mOrgName) {
			$result[] = array ($this->mOrgUrl, $this->mOrgName, null);
		}
		return $result;
	}

	public function getSource () {
		return array ($this->mDescUrl, $this->getTitle());
	}

	public function getWikiSource () {
		return str_replace ('_ID_', FlinfoData::replaceBrackets($this->mId), $this->mSiteTemplate);
	}

	public function getWikiDescription () {
		return '{{Watermark}}'; // Most images are watermarked
	}

	private function getOverrideLicense () {
		if (!$this->mOverrides) {
			$this->mOverrides = $this->loadParamFile (self::BUGWOOD_ORGANIZATIONS);
		}
		$org = urldecode($this->mOrgId);
		if (isset ($this->mOverrides[$org])) {
			return array ('license' => $this->mOverrides[$org], 'org' => $this->mOrgName);
		}
		return array ();
	}

	private function getLicenseName ($url) {
		if (preg_match ('!^http://creativecommons.org/licenses/([^/]+)(/|$)!', $url, $matches)) {
			return 'CC-' . strtoupper($matches[1]);
		}
		return 'Unknown license';
	}

	public function getLicenses ($goodUser) {
		$tags = array ();
		$status = $this->mLicenseUrl ? $this->mLicenseUrl : 'Unknown license';
		if ($goodUser) {
			if ($this->mLicense) {
				$tags[] = $this->mLicense;
				$status = null;
				$tags[] = $this->getReviewTag();
			}
		}
		if ($status && $this->mOrgId) {
			// License not OK, check organization
			$override = $this->getOverrideLicense ($this->mOrgId);
			if ($override['license']) {
				$this->mOriginalLicense = $this->getLicenseName ($this->mLicenseUrl);
				$tags[] = $override['license'];
				$status = null;
				$tags[] = $this->getReviewTag();
			}
		}
		return array ($status, $tags, null);
	}

	protected function internalGetReviewTag () {
		return "licensereview";
	}

	private function getRawTitle () {
		$title = null;
		if ($this->mGenus && $this->mSpecies) {
			$title = ucfirst ($this->mGenus) . ' ' . $this->mSpecies;
			if ($this->mAuthority) {
				$title .= ' ' . $this->mAuthority;
			}
		} else if ($this->mCommonName) {
			$title = ucfirst($this->mCommonName);
		}
		return $title;
	}

	public function getTitle () {
		$title = $this->getRawTitle();
		if (!$title) $title = $this->mSiteName . ' ' . $this->mId;
		return $title;
	}

	public function getUploadTitle () {
		$title = $this->getRawTitle();
		if (!$title) $title = $this->mSiteName;
		$title .= ' ' . $this->mId;
		return $title;
	}

	public function getDescription () {
		$desc = '';
		$descriptor = $this->mDescriptor;
		if ($this->mCommonName) {
			$desc .= ucfirst($this->mCommonName);
			if ($descriptor) {
				$desc .= ' (' . $descriptor . ')';
				$descriptor = null;
			}
			$desc .= "\n";
		}
		if ($this->mGenus) {
			if ($this->mCommonName) $desc .= '<br />';
			$desc .= '<b><i>' . $this->mGenus . ' ' . $this->mSpecies . '</i></b>' . ($this->mAuthority ? ' ' . $this->mAuthority : "");
			if ($descriptor) {
				$desc .= ' (' . $descriptor . ')';
			}
			$desc .= "\n";
		}
		if ($this->mDescription) {
			$desc .= "\n" . $this->mDescription . "\n";
		}
		if ($this->mLocation) {
			$desc .= "\nImage location: " . $this->mLocation;
		}
		return $desc;
	}

	public function getPermission () {
		if ($this->mOriginalLicense) {
			$text =
				str_replace (
					 array ('_LICENSE_', '_PROGRAM_', '_REPO_', '_FIELD_', '_VAL_')
					,array ($this->mOriginalLicense, FlinfoGlobals::$title, $this->mNiceSiteName, 'Organization', $this->mOrgName)
					,FlinfoData::msg('license_override_tag')
				);
			return array (null, $text);
		}
		return array (null, null);
	}

	public function getCategories () {
		$tags = array ();
		if ($this->mGenus) $tags[] = $this->mGenus;
		if ($this->mCommonName) $tags[] = ucfirst($this->mCommonName);
		if ($this->mSpecies) $tags[] = $this->mGenus . ' ' . $this->mSpecies;
		if ($this->mLocation) {
			// Try to build a few sensible locations
			$tmp = explode (',', $this->mLocation);
			foreach ($tmp as $loc) {
				$tags[] = trim ($loc);
			}
			$tmp = explode (';', $this->mLocation);
			foreach ($tmp as $loc) {
				$tags[] = trim ($loc);
			}
		}
		return $tags;
	}

	public function filterCategories ($categories) {
		// Remove any categories that are prefixes of other categories. (Happens often with "Genus" and "Genus species".)
		if (!$categories) {
			if ($this->mGenus) {
				if ($this->mSpecies) {
					$this->mWarnMissingSpecies = $this->mGenus . ' ' . $this->mSpecies;
				} else {
					$this->mWarnMissingSpecies = $this->mGenus;
				}
			}
			return array();
		}
		$map = array_fill_keys ($categories, true);
		sort($categories);
		$length = count ($categories);
		for ($i = 0; $i + 1 < $length; $i++) {
			if (strpos ($categories[$i+1], $categories[$i]) === 0) {
				unset($map[$categories[$i]]);
			}
		}
		// Check whether Genus and/or species are recognized as categories. If not, we'll use the htmlHook to generate a warning later on.
		if ($this->mGenus) {
			$genus = 'Category:' . $this->mGenus;
			$species = null;
			if ($this->mSpecies != 'sp.' && $this->mSpecies != 'spp.') {
				$species = $genus . ' ' . $this->mSpecies;
			}
			if ($species) {
				if (!isset($map[$species])) {
					$this->mWarnMissingSpecies = $species;
				}
			} else if (!isset($map[$genus])) {
				$this->mWarnMissingSpecies = $genus;
			}
			if ($this->mWarnMissingSpecies) {
				$this->mWarnMissingSpecies = substr ($this->mWarnMissingSpecies, 9);
			}
		}
		return array_keys($map);
	}

	public function htmlHook (&$htmlText) {
		if ($this->mWarnMissingSpecies) {
			$html = FlinfoHtmlGen::openElement ('div', array ('style' => 'background:pink;'));
			$html .= FlinfoHtmlGen::text(str_replace ('_SPECIES_', $this->mWarnMissingSpecies, FlinfoData::msg('missing_species')));
			$html .= FlinfoHtmlGen::close ('div');
			$htmlText .= $html;
		}
		if ($this->mOriginalLicense) {
			$override = $this->getOverrideLicense();
			$html = FlinfoHtmlGen::openElement ('div', array ('style' => 'background:pink;'));
			$html .= FlinfoHtmlGen::text(
				str_replace (
					 array ('_LICENSE_', '_PROGRAM_', '_REPO_')
					,array ($this->mOriginalLicense, FlinfoGlobals::$title, $this->mNiceSiteName)
					,FlinfoData::msg('license_override')
				)
			);
			$html .= FlinfoHtmlGen::close ('div');
			$htmlText .= $html;
		}
		return true;
	}

	public function getDate () {
		return $this->mDate;
	}

	public function getDateFormat () {
		return "%Y-%m-%d";
	}

	public function getSizes () {
		return $this->mSizes;
	}

	/**
	 * Fake a JSON server result and return that.
	 *
	 * @return Faked "raw" server result.
	 */
	public function getRawResult () {
		if ($this->mStatus === null) return null;
		$result = array ("status" => $this->mStatus, "raw_id" => $this->mRawId);
		$photo = null;
		if ($this->mId !== null) {
			$photo = array ();
			$photo["id"] = $this->mId;
		}
		if ($this->mStatus == FlinfoStatus::STATUS_OK) {
			$photo["url"] = $this->mDescUrl;
			$photo["title"] = $this->getTitle();
			$photo["owner"] = array ("userId" => $this->mUserId, "userName" => $this->mUserName, "userPage" => $this->mUserUrl);
			$photo["license"] = array ("key" => $this->mLicense, "link" => $this->mLicenseUrl);
			if ($this->mDate) $photo["date"] = $this->mRawDate;
			$photo["sizes"] = $this->mSizes;
		}
		if ($photo) $result["photo"] = $photo;
		return $result;
	}

}