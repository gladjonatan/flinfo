<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 *  Simple class to encapsulate data directories and access to messages.
 */
abstract class FlinfoData {

	private static $msgs = null;
	private static $language = null;

	/**
	 * Get the data directory of Flinfo. This must be writeable by php.
	 *
	 * @return String
	 */
	public static function dataDirectory() {
		return dirname(__FILE__) . "/data";
	}

	private static $gpcDone = false;

	/**
	 * PHP has this dreadful "magic quoting" feature that adds slashes to request arguments.
	 * This function undoes this for the request globals $_GET, $_POST, $_REQUEST, $_COOKIE
	 * if necessary and switches runtime magic quoting off, such that e.g. file_get_contents
	 * *really* returns what it should.
	 *
	 * @return void
	 */
	public static function turnOffMagicQuoting() {
		// This whole magic quoting stuff is deprecated as of PHP 5.3, so carefully test
		// whether the function exists before using it.
		if (!self::$gpcDone && function_exists ('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
			// According to the docu of the deprecated 'magic_quotes_sybase' at
			// http://www.php.net/manual/en/sybase.configuration.php#ini.magic-quotes-sybase
			// the stripslashes and addslashes functions honor this setting and thus do not add
			// or strip slashes but simply duplicate all single quotes or replace two consecutive
			// single quotes by single single quotes. Hence we do not need to test whether or not
			// this flag is set; we can simply use stripslashes to undo whatever was done when
			// the request variables were set up.
			$vars = array (&$_GET, &$_POST, &$_REQUEST, &$_COOKIE);
			foreach ($vars as &$v) {
				foreach ($v as $k => &$e) {
					if (is_string($e)) $e = stripslashes ($e);
				}
			}
			self::$gpcDone = true;
		}
		// Switch them off, if we can! magic_quotes_gpc cannot be switched off, it's a PERDIR setting.
		if (function_exists ('set_magic_quotes_runtime')) @set_magic_quotes_runtime (false); // Deprectaed since PHP 5.3.0
		ini_set ('magic_quotes_runtime', 0);
		ini_set ('magic_quotes_sybase', null);
	}

	/**
	 * Get a UI message text.
	 *
	 * @param String $key Message key
	 * @return String Message text, or null if there is no message for $key.
	 */
	public static function msg($key) {
		if (self::$msgs === null) {
			self::$msgs = self::get_messages(self::userLanguage());
		}
		if (isset (self::$msgs[$key])) {
			return self::$msgs[$key];
		}
		return null;
	}

	/**
	 * Get the UI language.
	 *
	 * @return String language code
	 */
	public static function userLanguage() {
		if (self::$language === null) {
			self::$language = self::get_language();
		}
		return self::$language;
	}

	/**
	 * Replace message references __MESSAGE__key__ in a string by the corresponding message.
	 *
	 * Any occurrence of such a string will be replaced by the result of FlinfoData::msg(key).
	 * If no such message exists, the message reference is not replaced.
	 *
	 * @param string $str String possibly containing message references.
	 * @return string Input string with all message references replaced by the corresponding messages.
	 */
	public static function substMessages ($str) {
		$split = explode ('__MESSAGE__', $str);
		$items = count ($split);
		if ($items > 1) {
			for ($i = 1; $i < $items; $i++) {
				$e = explode ('__', $split[$i], 2);
				if (count($e) > 1) {
					$msg = self::msg($e[0]);
					if ($msg === null) {
						$e[0] = '__MESSAGE__' . $e[0] . '__'; // No such message
					} else {
						$e[0] = $msg;
					}
					$split[$i] = implode ('', $e);
				} else {
					$split[$i] = '__MESSAGE__' . $split[$i];
				}
			}
			return implode ('', $split);
		}
		return $str;
	}

	/**
	 * Utility function to replace square brackets, which may cause troubles inside wikilinks.
	 *
	 * @param string  $str    The string to convert
	 * @param boolean $forUrl if true, use %-encoding, otherwise HTML character entities.
	 *
	 * @return string The converted string.
	 */
	public static function replaceBrackets ($str, $forUrl = null)
	{
		if (!$forUrl) {
			$result = str_replace (
				  array ('[', ']', '{', '|', '}')
				, array ('&#x5B;', '&#x5D;', '&#x7B;', '&#x7C;', '&#x7D;')
				, $str);
		} else {
			$result = str_replace (
				  array ('[', ']', '{', '|', '}', ' ')
				, array ('%5B', '%5D', '%7B', '%7C', '%7D', '%20')
				, $str);
		}
		return $result;
	}

	/**
	 * Create an external link in MediaWiki syntax (single square brackets).
	 *
	 * @param string $url
	 * @param string $title
	 * @return string The wikilink '[$url $title]', with special characters appropriately replaced.
	 */
	public static function makeWikiLink ($url, $title) {
		if (!$title || $title == "") {
			return '[' . self::replaceBrackets ($url, true) . ']';
		}
		return '[' . self::replaceBrackets ($url, true) . ' ' . self::replaceBrackets ($title) . ']';
	}

	/**
	 * Determine the user interface language. If not set explicitly in the request, try to figure out the
	 * browser's default language. In any failure cases, fall back to English as the default.
	 *
	 * @return String language code
	 */
	private static function get_language()
	{
		$user_lang = isset($_REQUEST['user_lang']) ? $_REQUEST['user_lang'] : "";

		if ($user_lang == "") {
			$user_lang = 'en';
			$acceptLang = isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) ? $_SERVER["HTTP_ACCEPT_LANGUAGE"] : "";
			//http://www.php-resource.de/forum/showthread.php?threadid=22545
			if (preg_match('/^([a-z]+)-?([^,;]*)/i', $acceptLang, $matches)) {
				$user_lang = $matches[1];
				if($user_lang == "") {
					$user_lang = 'en';
				}
			}
		}
		// Make sure it is only letters, dashes and underscores, and at most 15 characters. 15 is arbitrary.
		if (strlen($user_lang) > 15 || !preg_match ('/^[-_a-z]*$/i', $user_lang)) {
			$user_lang = 'en';
		}
		return $user_lang;
	}

	/**
	 * Include the appropriate message file, which is supposed to populate the $messages array.
	 * If there is no file for the specified language code, or if it lacks some messages, fall
	 * back to using English messages.
	 *
	 * @param String $lang Language code
	 * @return array containing the messages
	 */
	private static function get_messages($lang)
	{
		$messages = array();
		$inc_dir = dirname(__FILE__) . "/lang";

	    include("$inc_dir/en.php"); // Must exist
		if ($lang != 'en') {
			$fName = "$inc_dir/$lang.php";
			if ((dirname ($fName) == $inc_dir) && file_exists($fName)) {
				include($fName);
			}
		}
		return $messages;
	}
}
