<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoStatus.php');
require_once ('FlinfoGlobals.php');
require_once ('FlinfoHooks.php');
require_once ('FlinfoData.php');
require_once ('lib/Curly.php');

require_once ('FlinfoIn.php');
require_once ('FlinfoBugwood.php');
require_once ('FlinfoFlickr.php');
require_once ('FlinfoIpernity.php');
require_once ('FlinfoMushroom.php');
require_once ('FlinfoPanoramio.php');
require_once ('FlinfoPicasa.php');
require_once ('FlinfoLeMill.php');
// Add further input modules here, and enter them in the array below

/**
 * Main class of Flinfo, reading data from various servers and constructing
 * an intermediary representation.
 */
class FlinfoWiki {

	/**
	 * The image ID as determined by $mFacade.
	 *
	 * May be null if no id at all was given.
	 * @var unknown_type
	 */
	protected $mId;

	/**
	 * True if not giving an id is allowed (generate will just return null),
	 * false if it's an error (generate will return a result with the status
	 * FlinfoStatus::STATUS_MISSING_ID).
	 *
	 * @var boolean
	 */
	protected $mAllowNoId;

	/**
	 * The image repository facade used
	 *
	 * @var FlinfoIn
	 */
	protected $mFacade;

	/**
	 * Status indicator for the calls to the actual image repository server (like
	 * flickr). Can be one of
	 * - FlinfoStatus::STATUS_OK
	 * - FlinfoStatus::STATUS_SERVER_FAILURE
	 * - FlinfoStatus::STATUS_INTERNAL_ERROR
	 * - FlinfoStatus::STATUS_INVALID_ID
	 * @var int
	 */
	protected $mServerStatus;

	/**
	 * The name of the repository used. Returned by generate in $result['repo'].
	 * If that is not equal to the repository name given in the request (if any),
	 * the caller knows that the repositor he specified is unknown, and he also
	 * knows which repository was used (flickr by default).
	 *
	 * @var string
	 */
	protected $mRepo;

	private $mDesc;
	private $mCats;
	private $mStopEarly;

	/**
	 * Either null, if the license is Commons-compatible, or a message describing
	 * the license.
	 *
	 * @var string
	 */
	private $mLicenseStatus = null;

	/**
	 * Ini file that may contain regexps describing texts to be removed from descriptions.
	 * @var string
	 */
	const STRIP_FILE = 'strip_texts.ini';

	/**
	 * Class constructor
	 *
	 * @param array   $requestParams pass in $_REQUEST
	 * @param boolean $allowNoId     true if not giving an id is allowed
	 */
	public function __construct ($requestParams, $allowNoId, $mayStopEarly) {
		$this->mAllowNoId = $allowNoId;
		$this->mStopEarly = $mayStopEarly;
		if (isset ($requestParams['id']) && $requestParams['id'] != null && $requestParams['id'] != "") {
			$id = trim ($requestParams['id']);
			$this->mFacade = $this->createFacade ($requestParams, $id);
			list ($parsedId, $status) = $this->mFacade->getInfo ($id);
			$this->mId = $parsedId;
			$this->mServerStatus = $status;
		} else {
			$this->mId = null;
			if (isset ($requestParams['repo'])) {
				$repo = $requestParams['repo'];
				if (isset ($this->mFacadeDescriptions[$repo])) {
					$this->mRepo = $repo;
				}
			}
			$this->mServerStatus = FlinfoStatus::STATUS_OK;
		}
		$this->mDesc  = isset($requestParams['description']) ? $requestParams['description'] : null;
		$this->mCats  = isset($requestParams['categories']) ? $requestParams['categories'] : null;
	}

	/**
	 * Image repository facades. The key is the external repository name as
	 * expected in $_REQUEST['repo'] (optional). Each facade has the following
	 * info:
	 * - name    Class name of a subclass of FlinfoIn to instantiate
	 * - display Name to display for this repository
	 * - params  Simple filename of the parameter file for this facade
	 * - regexps Either a single string containing a regexp matching known URLs
	 *           for which this facade should be used, or an array of such
	 *           regexps.
	 * @var array
	 */
	private $mFacadeDescriptions =
	  array (
	    'flickr' =>
	       array (
	         'name'    => 'FlinfoFlickr'
	        ,'display' => 'Flickr'
	        ,'url'     => 'http://www.flickr.com/'
	        ,'params'  => 'flickrkey.txt'
	        ,'regexps' => '!^https?://([^.]+\.)*?flickr\.com/!'
	       )
	   ,'ipernity' =>
	       array (
	         'name'    => 'FlinfoIpernity'
	        ,'display' => 'ipernity'
	        ,'url'     => 'http://www.ipernity.com/'
	        ,'params'  => 'ipernitykey.txt'
	        ,'regexps' => '!^https?://([^.]+\.)*?ipernity\.com/!'
	       )
	   ,'panoramio' =>
	       array (
	         'name'    => 'FlinfoPanoramio'
	        ,'display' => 'panoramio'
	        ,'url'     => 'https://www.panoramio.com/'
	        ,'params'  => ""
	        ,'regexps' => '!^https?://.*?(([./]panoramio\.com\/)|/panoramio/|google.com/[^/]*panoramio/)!'
	       )
	   ,'picasa' =>
	   	   array (
	         'name'    => 'FlinfoPicasa'
	        ,'display' => 'Picasa'
	        ,'url'     => 'https://picasaweb.google.com/'
	        ,'params'  => ""
	        ,'regexps' => '!^https?://((picasaweb\.google\.[^/]+)|(lh[^.]*\.(ggpht|googleusercontent)\.com)|(plus\.google\..*?/photos/[^/]+/albums))/!'
	   	   )
	   ,'mushroom' =>
	   	   array (
	         'name'    => 'FlinfoMushroom'
	        ,'display' => 'Mushroom Observer'
	        ,'url'     => 'http://www.mushroomobserver.org/'
	        ,'params'  => ""
	        ,'regexps' => '!^https?://(((www\.)?mushroomobserver\.org/)|(images\.digitalmycology\.com/))!'
	   	   )
	   ,'insectimages' =>
	   	   array (
	         'name'    => 'FlinfoBugwood'
	        ,'display' => 'Insect Images'
	        ,'url'     => 'http://www.insectimages.org/'
	        ,'params'  => 'insectimages.txt'
	        ,'regexps' => '!^https?://(www\.)?(insectimages|bugwood)\.org/!'
	   	   )
	   ,'invasive' =>
	   	   array (
	         'name'    => 'FlinfoBugwood'
	        ,'display' => 'Invasive Species'
	        ,'url'     => 'http://www.invasive.org/'
	        ,'params'  => 'invasive.txt'
	        ,'regexps' => '!^https?://(www\.)?invasive\.org/!'
	   	   )
	   ,'weedimages' =>
	   	   array (
	         'name'    => 'FlinfoBugwood'
	        ,'display' => 'Weed Images'
	        ,'url'     => 'http://www.weedimages.org/'
	        ,'params'  => 'weedimages.txt'
	        ,'regexps' => '!^https?://(www\.)?weedimages\.org/!'
	   	   )
	   ,'ipmimages' =>
	   	   array (
	         'name'    => 'FlinfoBugwood'
	        ,'display' => 'IPM Images'
	        ,'url'     => 'http://www.ipmimages.org/'
	        ,'params'  => 'ipmimages.txt'
	        ,'regexps' => '!^https?://(www\.)?ipmimages\.org/!'
	   	   )
	   ,'forestryimages' =>
	   	   array (
	         'name'    => 'FlinfoBugwood'
	        ,'display' => 'Forestry Images'
	        ,'url'     => 'http://www.forestryimages.org/'
	        ,'params'  => 'forestryimages.txt'
	        ,'regexps' => '!^https?://(www\.)?forestryimages\.org/!'
	   	   )
	   ,'lemill' =>
	   	   array (
	         'name'    => 'FlinfoLeMill'
	        ,'display' => 'LeMill'
	        ,'url'     => 'http://lemill.net/'
	        ,'params'  => ""
	        ,'regexps' => '!^http://(www\.)?lemill\.net/!'
	   	   )
	  );

	public final function getFacadeNames () {
		$result = array ();
		foreach ($this->mFacadeDescriptions as $k => $f) {
			$result[] = array ('text' => $f['display'], 'title' => $f['url'], 'value' => $k);
		}
		return $result;
	}

	/**
	 * Factory function for repository facades. If no facade can be determined
	 * for the given id, flickr is used by default. Sets $this->mRepo as a
	 * side-effect.
	 *
	 * @param $requestParams passed though $_REQUEST
	 * @param $id            id of the image to look for
	 * @return FlinfoIn
	 */
	private function createFacade ($requestParams, $id) {
		// Try matching the id against the regexps given for each repo.
		// If we have a match, that's the repo to use.
		foreach ($this->mFacadeDescriptions as $k => $r) {
			if (is_array ($r['regexps'])) {
				foreach ($r['regexps'] as $regExp) {
					if (!preg_match ($regExp, $id)) continue;
					$this->mRepo = $k;
					return new $r['name'] ($r['params'], $requestParams);
				}
			} else {
				if (preg_match ($r['regexps'], $id)) {
					$this->mRepo = $k;
					return new $r['name'] ($r['params'], $requestParams);
				}
			}
		}
		// No match. Check whether an explicit repository is set
		if (isset ($requestParams['repo'])) {
			$repo = $requestParams['repo'];
			if (isset ($this->mFacadeDescriptions[$repo])) {
				$this->mRepo = $repo;
				return new $this->mFacadeDescriptions[$repo]['name'] ($this->mFacadeDescriptions[$repo]['params'], $requestParams);
			}
		}
		// If no repo matches, use Flickr as the default
		$this->mRepo = 'flickr';
		return new $this->mFacadeDescriptions['flickr']['name'] ($this->mFacadeDescriptions['flickr']['params'], $requestParams);

	}

	/**
	 * Return the image ID as determined by the server facade.
	 *
	 * @return int
	 */
	public function getId () {
		return $this->mId;
	}

	/**
	 * Main generation routine, crreating the intermediary representation.
	 *
	 * @return array Intermediary representation of the result.
	 */
	public final function generate () {
		$result = array();
        $result['repo'] = $this->mRepo;
		if ($this->mId == null) {
			if ($this->mAllowNoId) {
				return null;
			} else {
				$result['status'] = FlinfoStatus::STATUS_MISSING_ID;
				return $result;
			}
		}
		// We had an ID so we should have gotten something.
		if ($this->mServerStatus != FlinfoStatus::STATUS_OK) {
			$result['status'] = $this->mServerStatus;
			return $result;
		}
		// Fields to set: description, source, author, date, permission, license, categories, geoinfo
		$result['status'] = FlinfoStatus::STATUS_OK;
		$info = array();
		// Author (with blacklist check)
		$goodUser = $this->mFacade->verifyUser ();
		if (!$goodUser) {
			$result['status'] = FlinfoStatus::STATUS_BAD_USER;
		}
		list ($licenseStatus, $licenses, $additionalSource) = $this->mFacade->getLicenses ($goodUser);
		if (!$goodUser) {
			$licenses[] = FlinfoData::msg('speedy_template');
		} else if ($licenseStatus != null) {
			$result['status'] = FlinfoStatus::STATUS_BAD_LICENSE;
			$copyviotext = str_replace ('_LICENSE_NAME_', $licenseStatus, FlinfoData::msg('copyvio_reason'));
			$licenses[] = FlinfoData::msg('copyvio_template') . "|reason=$copyviotext --~~~~";
			$this->mLicenseStatus = $copyviotext;
		} else {
			$this->mLicenseStatus = null; // License and user OK
		}
		if ($this->mStopEarly && $result['status'] != FlinfoStatus::STATUS_OK) {
			// Don't do the whole rest if we can avoid it.
			$stopEarly = true;
			FlinfoHooks::run ('flinfoStopEarly', array (&$stopEarly));
			if ($stopEarly) {
				return $result;
			}
		}
		$info['author'] = $this->mFacade->getWikiAuthor ();
		if (!$info['author']) {
			$authors = $this->mFacade->getAuthor ();
			$text = array ();
			foreach ($authors as $author) {
				if ($author[0]) {
					$text[] = FlinfoData::makeWikiLink ($author[0], $author[1]) . ($author[2] ? ' from ' . $author[2] : "");
				} else {
					$text[] = $author[1] . ($author[2] ? ' from ' . $author[2] : "");
				}
			}
			$info['author'] = implode ("\n\n", $text);
		}
		$time = $this->mFacade->getDate ();
		$currentTime = time ();
		$info['date'] = "";
		if ($time > 0 && $time <= time()) {
			$info['date'] = strftime($this->mFacade->getDateFormat(), $time);
		}
		$info['source'] = $this->mFacade->getWikiSource ();
		if (!$info['source']) {
			list ($imageUrl, $imageTitle) = $this->mFacade->getSource ();
			$info['source'] = FlinfoData::makeWikiLink ($imageUrl, $imageTitle);
		}
		if ($goodUser && $licenseStatus == null && $additionalSource) {
			$info['source'] .= "\n$additionalSource";
		}
		// Description
		$info['desc'] = $this->getDescription ();
		// Permission
		list ($permissionLink, $owner) = $this->mFacade->getPermission ();
		if ($permissionLink != null) {
			$info['permission'] = FlinfoData::makeWikiLink ($permissionLink, $owner);
		} else {
			$info['permission'] = $owner ? $owner : "";
		}
		$result['info'] = $info;
		// GeoInfo
		$geoInfo = $this->mFacade->getGeoInfo ();
		if ($geoInfo != null) {
			$result['geolocation'] = $geoInfo;
		}
		// Categories
		$categories = $this->getCategories ();
		if ($categories != null) {
			$result['categories'] = $categories;
		}
		$result['licenses'] = $licenses;
		return $result;
	}

	/**
	 * Get some additional info useful for building a user interface.
	 *
	 * @return array
	 */
	public final function getDisplayInfo ()
	{
		$result = array ();
		if ($this->mFacade) {
			// For the preview, we need image page url, title, and a url to a thumbnail
			list ($imagePage, $imageTitle) = $this->mFacade->getSource ();
			$alternateSource = $this->mFacade->getAlternateSource();
			$availableSizes = $this->mFacade->getSizes ();
			// For the full upload, we need a link to the largest version available.
			$result['desc'] = $imagePage;
			$result['title'] = $imageTitle;
			$result['uploadTitle'] = $this->mFacade->getUploadTitle ();
			$result['sizes'] = $availableSizes;
			$result['repo'] = $this->mRepo;
			$serverError = $this->mFacade->getServerError ();
			$result['error'] = is_string ($serverError) ? $serverError : '';
			if ($this->mLicenseStatus) $result['licenseStatus'] = $this->mLicenseStatus;
			$result['alreadyFound'] = $this->getAlreadyThere($imagePage, $alternateSource);
			if ($availableSizes) {
				$last = count($availableSizes) - 1;
				if ($last >= 0) {
					$result['portrait'] = $availableSizes[$last]['width'] < $availableSizes[$last]['height'];
				}
			}
		} else {
			if ($this->mRepo) {
				$result['repo'] = $this->mRepo;
			}
		}
		return $result;
	}

	/**
	 * Get the raw result as returned by the repository.
	 *
	 * @return array
	 */
	public final function getRawServerResult ()
	{
		if ($this->mFacade) {
			return $this->mFacade->getRawResult ();
		}
		return null;
	}

	public static function makeWikiLink ($url, $text) {
		$text = trim ($text);
		if (preg_match ('!^(https?://(www\.)?flickr\.com/)(x/t/[^/]+/)(photos/.*)$!', $url, $matches)) {
			// Remove Flickr clicktracking
			if ($text == $url) $text = $matches[1] . $matches[4];
			$url = $matches[1] . $matches[4];
		}
		return FlinfoData::makeWikiLink ($url, $text);
	}

	/**
	 * Read and parse our custom ini files. parse_ini_file does unfortunately not support
	 * assigning associative arrays (foo["someKey"] = ...)
	 *
	 * @param string $fileName
	 * @return array
	 */
	private function loadTexts ($fileName) {
		if (!is_file ($fileName)) return null;
		$contents = file_get_contents ($fileName);
		if (!$contents) return null;
		$contents = explode ("\n", $contents);
		$result = array ();
		$section = null;
		foreach ($contents as $line) {
			$line = trim ($line);
			if ($line == '' || substr ($line, 0, 1) == '#') continue;
			if (substr ($line, 0, 1) == '[') {
				$endPos = strrpos ($line, ']');
				$newSection = ($endPos === false) ? substr ($line, 1) : $newSection = substr ($line, 1, $endPos-1);
				if ($newSection != '') $section = $newSection;
			}
			$line = explode ('=', $line, 2);
			if (count ($line) !== 2) continue;
			$line[0] = trim ($line[0]);
			$line[1] = trim ($line[1]);
			if ($line[1] == '') continue;
			if ($section) {
				if (!isset ($result[$section])) $result[$section] = array ();
				if (is_array($result[$section])) {
					$result[$section][$line[0]] = $line[1];
				}
			} else {
				if (!isset ($result[$line[0]]) || !is_array($result[$line[0]])) {
					$result[$line[0]] = $line[1];
				}
			}
		}
		return (count ($result) > 0) ? $result : null;
	}

	/**
	 * Return the description, converted into wikitext. Links and such are rewritten in wiki-syntax.
	 * If the request specified a description, that overrides a Flickr description.
	 *
	 * @return String The description in wiki syntax.
	 */
	private function getDescription ()
	{
		if ($this->mDesc) return $this->mDesc;
		$desc = $this->mFacade->getDescription ();
		if (!$desc || $desc == "") {
			$desc = $this->mFacade->getTitle ();
			if ($desc && $desc != "") {
				$desc = str_replace ('{', '&#x7B;', $desc);
				$desc = str_replace ('|', '&#x7C;', $desc);
				$desc = str_replace ('}', '&#x7D;', $desc);
			} else {
				$desc = "";
			}
			return $desc;
		}
		// Replace CR-LF
		$desc = str_replace ("\r\n", "\n", $desc);
		$toIgnore = $this->loadTexts (FlinfoData::dataDirectory() . '/' . self::STRIP_FILE);
		if (is_array ($toIgnore) && isset ($toIgnore[$this->mRepo])) {
			$account = $this->mFacade->getAccountId ();
			if ($account && isset($toIgnore[$this->mRepo][$account])) {
				$desc = preg_replace($toIgnore[$this->mRepo][$account], '', $desc);
			}
			// Also apply defaults
			if (isset($toIgnore[$this->mRepo][''])) {
				$desc = preg_replace($toIgnore[$this->mRepo][''], '', $desc);
			}
			if (isset($toIgnore[''])) {
				$desc = preg_replace($toIgnore[''], '', $desc);
			}
		}
		// HTML links to wikilinks
		$desc =
		  preg_replace_callback (
			  '!<a\s[^>]*href\s*=\s*["\'](.*?)["\'][^>]*>(.*?)</a>!ims'
			, create_function (
				  '$matches'
				, 'return FlinfoWiki::makeWikiLink ($matches[1], $matches[2]);'
			  )
			, $desc
		  );
		$desc = str_replace ('<strong>', "'''", $desc);
		$desc = str_replace ('</strong>', "'''", $desc);
		$desc = str_replace ('<b>', "'''", $desc);
		$desc = str_replace ('</b>', "'''", $desc);
		$desc = str_replace ('<i>', "''", $desc);
		$desc = str_replace ('</i>', "''", $desc);
		$desc = str_replace ('<em>', "''", $desc);
		$desc = str_replace ('</em>', "''", $desc);
		$desc = str_replace ('{', '&#x7B;', $desc);
		$desc = str_replace ('|', '&#x7C;', $desc);
		$desc = str_replace ('}', '&#x7D;', $desc);
		$extra = $this->mFacade->getWikiDescription();
		if ($extra) {
			$desc .= "\n" . $extra;
		}
		$this->mDesc = $desc;
		return $desc;
	}

	/**
	 * Return a normalized array of categories (which may be empty) given the Flickr tags. Note:
	 * if categories were given in the request, these override any Flickr tags.
	 *
	 * @param: $flickr Flickr info
	 * @return array Normalized array of categories.
	 */
	private function getCategories ()
	{
		if ($this->mCats) {
			$cats = explode (';', $this->mCats);
			foreach ($cats as $key => $cat) {
				$cats[$key] = strtoupper (substr ($cat, 0, 1)) . substr ($cat, 1);
			}
		} else {
			$cats = $this->mFacade->getCategories ();
		}
		$result = $this->verifyCategories ($cats);
		$result = $this->mFacade->filterCategories ($result);
		return $result;
	}

	const CATEGORY_OK    = 0;
	const CATEGORY_REDIR = 1;
	const CATEGORY_BAD   = 2;

	/**
	 * Convert the input array of categories into a normalized output array. Normalized means
	 * that any category redirects are resolved and  non-existsing categories, some meta-categories,
	 * and disambiguation categories are removed. The result does not contain duplicates.
	 *
	 * @param array $cats Possibly empty array of category candidates
	 * @return array      Normalized categories.
	 */
	private function verifyCategories ($cats)
	{
		if (!is_array ($cats) || count($cats) == 0) return $cats;
		$catstring = "Category:" . implode ('|Category:', $cats);
		// pllimit (Link limit) is per page, but cllimit (Category limit) is global over all pages! Just an idiosyncrasy of the Commons API...
		// Hm, apparently no longer.
		$baseUrl = 'https://commons.wikimedia.org/w/api.php?action=query&rawcontinue=1&format=php&prop=info|links|categories&plnamespace=14&pllimit=' . (count($cats) * 100) . '&cllimit=' . (count($cats) * 10) . '&titles=';
		$url = $baseUrl . rawurlencode ($catstring);
		$result = array();
		$data = Curly::getContents($url, FlinfoGlobals::USER_AGENT);
		$arr = $data ? unserialize($data) : null;
		if (!$arr || !isset ($arr['query']) || !isset ($arr['query']['pages']) || !is_array ($arr['query']['pages'])) {
			return $result;
		}
		$redirs = array();
		$keys = array_keys($arr['query']['pages']);
		foreach($keys as $key) {
			if ($key > 0) {
				$catStatus = $this->checkOneCategory ($arr['query']['pages'][$key]);
				if ($catStatus == self::CATEGORY_REDIR) {
					// First link to a category:
					if (isset ($arr['query']['pages'][$key]['links'])) {
						foreach ($arr['query']['pages'][$key]['links'] as $l) {
							if ($l['ns'] == 14) {
								$redirs[] = $l['title'];
								break;
							}
						}
					}
				} else if ($catStatus == self::CATEGORY_OK) {
					$result[] = $arr['query']['pages'][$key]['title'];
				}
			}
		}
		if (count ($redirs) > 0) {
			// We resolved some redirects. Check those again (for double redirs, or redirs to "don't use" categories,
			// or redirs to non-existing categories).
			$catstring = implode ('|', $redirs);
			$url = $baseUrl . rawurlencode ($catstring);
			$data = Curly::getContents($url, FlinfoGlobals::USER_AGENT);
			$arr = $data ? unserialize($data) : null;
			if ($arr && isset($arr['query']) && isset($arr['query']['pages']) && is_array ($arr['query']['pages'])) {
				$keys = array_keys($arr['query']['pages']);
				foreach($keys as $key) {
					if ($key > 0) {
						$catStatus = $this->checkOneCategory ($arr['query']['pages'][$key]);
						if ($catStatus == self::CATEGORY_OK) {
							$result[] = $arr['query']['pages'][$key]['title'];
						}
					}
				}
			}
		}
		// Now remove duplicates. (Could have been created e.g. by redirect resolution.)
		if (count ($result) > 0) {
			$result = array_keys (array_fill_keys ($result, true));
		}
		return $result;
	}

	/**
	 * Check one single category for being a redirect or a category to be skipped.
	 *
	 * @param $page The page info from the result of the Commons API query
	 *
	 * @return int - CATEGORY_OK, CATEGORY_REDIR, or CATEGORY_BAD
	 */
	private function checkOneCategory ($page)
	{
		if (isset ($page['redirect'])) {
			return self::CATEGORY_REDIR; // Hard redirect
		}
		if (isset ($page['categories'])) {
			$categories = $page['categories'];
			foreach ($categories as $c) {
				switch (strtolower($c['title'])) {
					case 'category:disambiguation':
					case 'category:categories requiring permanent diffusion':
						return self::CATEGORY_BAD;
					case 'category:category redirects':
						return self::CATEGORY_REDIR; // Soft redirect
					default:
						break;
				}
			}
		}
		return self::CATEGORY_OK;
	}

	/**
	 * Ask the Commons whether any file page already links to any of these source URLs (minus the protocol) and returns the result.
	 * 
	 * @param String $source
	 * @param String $alternateSource if non-null, a second source link to try.
	 * 
	 * @return array of query results, with items 'title' and 'url' (of the page at the commons) and 'thumburl'. null if no results found.
	 */
	private function getAlreadyThere($source, $alternateSource)
	{
		$result = $this->getAlreadyThereOne($source);
		if ($result === null && $alternateSource !== null && strcmp ($source, $alternateSource) !== 0) {
			$result = $this->getAlreadyThereOne($alternateSource);
		}
		return $result;
	}

	/**
	 * Ask the Commons whether any file page already links to this source URL (minus the protocol) and returns the result.
	 * 
	 * @param String $source
	 * 
	 * @return array of query results, with items 'title' and 'url' (of the page at the commons). null if no results found.
	 */
	private function getAlreadyThereOne($source)
	{
		$baseUrl = 'https://commons.wikimedia.org/w/api.php?action=query&rawcontinue=1&format=php&generator=exturlusage&geunamespace=6&geuquery=';
		// Strip protocol, if any
		$idx = strpos($source, '://');
		$protocol = 'http';
		$otherProtocol = 'https';
		if ($idx !== false) {
			$protocol = strtolower(substr($source, 0, $idx));
			$source = substr($source, $idx + 3);
			if ($protocol == 'https') {
				$otherProtocol = 'http';
			} else if ($protocol != 'http') {
				$otherProtocol = null;
			}
		}
		$url = $baseUrl . rawurlencode($source) . '&prop=imageinfo&iiprop=url&iiurlwidth=100px';
		// The MediaWiki API for exturlusage is rather awkward; you cannot search for multiple protocols at once.
		$result = $this->extLinksQuery($url, $protocol);
		if ($result == null && $otherProtocol != null) {
			$result = $this->extLinksQuery($url, $otherProtocol);				
		}
		return $result;
	}

	private function extLinksQuery($url, $protocol) {
		$data = Curly::getContents($url .'&geuprotocol=' . $protocol, FlinfoGlobals::USER_AGENT);
		$arr = $data ? unserialize($data) : null;
		if (!$arr || !isset ($arr['query']) || !isset ($arr['query']['pages']) || !is_array ($arr['query']['pages'])) {
			return null;
		}
		$result = array();
		// Create items as we need them
		foreach ($arr['query']['pages'] as $item) {
			if (isset($item['imageinfo'])) {
				$result[] = array('title' => $item['title'], 'url' => $item['imageinfo'][0]['descriptionurl'], 'thumburl' => $item['imageinfo'][0]['thumburl']);
			} else {
				$result[] = array('title' => $item['title'], 'url' => 'https://commons.wikimedia.org/w/index.php?title='. rawurlencode($item['title']), 'thumburl' => null);
			}
		}
		if (count($result) == 0) return null;
		return $result;
	}
}