<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoData.php');
require_once ('FlinfoGlobals.php');
require_once ('FlinfoHooks.php');
require_once ('FlinfoStatus.php');
require_once ('FlinfoHtmlGen.php');
require_once ('lib/FormatJson.php');

/**
 * Flinfo output handlers.
 */
abstract class FlinfoOut {

	protected $mId;

	private static $failureMsg = array (
	    FlinfoStatus::STATUS_OK => null
	  , FlinfoStatus::STATUS_SERVER_FAILURE => 'error_server'
	  , FlinfoStatus::STATUS_INTERNAL_ERROR => 'error_internal'
	  , FlinfoStatus::STATUS_BAD_LICENSE => 'error_nonfree'
	  , FlinfoStatus::STATUS_BAD_USER => 'error_baduser'
	  , FlinfoStatus::STATUS_INVALID_ID => 'error_invalid_id'
	  , FlinfoStatus::STATUS_MISSING_ID => 'error_missing_id'
	);

	public function __construct ($params, $id)
	{
		$this->mId = $id;
	}

	public abstract function format ($wiki, $raw, $displayInfo, $repositories);

	protected function getErrorMsg ($errCode, $error)
	{
		if (isset (self::$failureMsg[$errCode]) && self::$failureMsg[$errCode]!== null) {
			$result = FlinfoData::msg(self::$failureMsg[$errCode]);
			$result = str_replace(array('_PHOTOID_','_FLINFO_SERVER_ERROR_'), array($this->mId, $error), $result);
			return $result;
		}
		return null;
	}

	/**
	 * Factory function to create output handlers. Plug in future handlers here.
	 *
	 * @param String $format identifies the desired handler.
	 * @param array $request $_REQUEST
	 * @param int $id The image id as determined by the input handler
	 * @return FlinfoOut output handler
	 */
	public static function create ($format, $request, $id) {
		// Create the output generator. Plug-in future output formatters here.
		switch ($format) {
			case 'json':
			case 'jsonfm':
				return new FlinfoJSON ($request, $id, $format == 'jsonfm');
			case 'php':
			case 'phpfm':
				return new FlinfoPhp ($request, $id, $format == 'phpfm');
			case 'raw':
				return new FlinfoRaw ($request, $id);
			case 'html':
			default:
				return new FlinfoHTML ($request, $id, FlinfoData::userLanguage());
		}
	}
}

abstract class FlinfoAPIOut extends FlinfoOut {

	private   $mName   = null;
	private   $mFilter = null;
	protected $mIsHtml = false;

	public function __construct ($params, $id, $name, $isHtml = false) {
		parent::__construct ($params, $id);
		$this->mIsHtml = $isHtml;
		$this->mName = $name;
		if (isset ($params['filter'])) {
			$this->mFilter = preg_split('/\s*,\s*/', trim ($params['filter']));
		} else {
			$this->mFilter = array ('wiki');
		}
	}

	public function format ($wiki, $raw, $displayInfo, $repositories) {
		$full = array ('wiki' => $wiki, 'raw' => $raw);
		$data = array ();
		foreach ($this->mFilter as $key) {
			if (isset ($full[$key])) {
				$data[$key] = $full[$key];
			}
		}
		$data = $this->encode ($data);
		if ($this->mIsHtml) {
        	header ('Content-Type: text/html; charset=utf-8');
        	echo FlinfoHtmlGen::docType() . "\n";
			echo FlinfoHtmlGen::open ('html') . "\n";
			echo FlinfoHtmlGen::open ('head') . "\n";
			echo FlinfoHtmlGen::element ('title', null, FlinfoGlobals::$title . ' - ' . $this->mName . ' output');
			echo FlinfoHtmlGen::close ('head') . "\n";
			echo FlinfoHtmlGen::open ('body') . "\n";
			echo FlinfoHtmlGen::text ('This is a HTML representation of the ' . $this->mName . ' result.') . "\n";
        	echo FlinfoHtmlGen::element('pre', null, $data);
        	echo FlinfoHtmlGen::close ('body');
        	echo FlinfoHtmlGen::close ('html');
		} else {
			$this->formatRaw ($data);
		}
	}

	protected abstract function formatRaw ($result);

	protected abstract function encode ($data);

} // end FlinfoAPIOut

class FlinfoJSON extends FlinfoAPIOut {

	private $mCallback;

	public function __construct ($params, $id, $isHtml = false) {
		parent::__construct ($params, $id, 'JSON', $isHtml);
		if (isset ($params['callback'])) {
			$this->mCallback = preg_replace ('/[^][.\'"_A-Za-z0-9\\\\]/', "", $params['callback']);
		} else {
			$this->mCallback = null;
		}
	}

	protected function encode ($data) {
		return FormatJson::encode ($data, $this->mIsHtml);
	}

	protected function formatRaw ($result) {
		if ($this->mCallback) {
			header ('Content-Type: text/javascript');
			echo $this->mCallback . "($result)";
		} else {
			header ('Content-Type: application/json');
			echo $result;
		}
	}

} // end FlinfoJSON

class FlinfoPhp extends FlinfoAPIOut {

	public function __construct ($params, $id, $isHtml = false) {
		parent::__construct ($params, $id, 'PHP', $isHtml);
	}

	protected function encode ($data) {
		if (!$this->mIsHtml) return serialize ($data);
		ob_start();
		var_dump ($data);
		return ob_get_clean();
	}

	protected function formatRaw ($result) {
		header ('Content-Type: application/vnd.php.serialized');
		echo $result;
	}

} // end FlinfoPhp

class FlinfoRaw extends FlinfoOut {

	public function __construct ($params, $id) {
		parent::__construct ($params, $id);
	}

	public function format ($wiki, $raw, $displayInfo, $repositories)
	{
		header ('Content-Type: text/plain; charset=utf-8');
		$errMsg = $this->getErrorMsg($wiki['status'], $displayInfo['error']);
		if ($errMsg !== null) {
			echo $errMsg;
			return;
		}
		$result = $this->createTextAreaContent ($wiki);
		echo $result;
	}

	protected function createTextAreaContent ($wiki)
	{
		if (!$wiki || !isset($wiki['info'])) return "";
		$result = "{{Information\n";
		$result .= "|Description=" . $wiki['info']['desc'] . "\n";
		$result .= "|Source=" . $wiki['info']['source'] . "\n";
		$extraSource = "";
		FlinfoHooks::run ('flinfoSource', array (&$extraSource));
		if ($extraSource != "" && is_string($extraSource)) {
			$result .= $extraSource;
		}
		$result .= "|Date=" . $wiki['info']['date'] . "\n";
		$result .= "|Author=" . $wiki['info']['author'] . "\n";
		$result .= "|Permission=" . $wiki['info']['permission'] . "\n";
		$result .= "|other_versions=\n";
		$result .= "}}\n";
		if (isset ($wiki['geolocation'])) {
			$result .= "{{Location dec|" . $wiki['geolocation']['latitude'] . "|" . $wiki['geolocation']['longitude'];
			if (isset ($wiki['geolocation']['source'])) $result .= "|source:" . $wiki['geolocation']['source'];
			$result .= "}}\n";
		}
		$result = str_replace("&amp;", "&", $result);
		$result = str_replace("&quot;", "\"", $result);

		// NARA tag, if any, is already in source.
		$result .= "\n=={{int:license-header}}==\n";
		$result .= "{{" . implode ("}}\n{{", $wiki['licenses']) . "}}\n";
		if (isset ($wiki['categories']) && count($wiki['categories']) > 0) {
			$result .= "\n[[" . implode ("]]\n[[", $wiki['categories']) . "]]\n";
		} else {
			$result .= "\n" . FlinfoData::msg('uncat') . "\n";
		}
		return $result;
	}

} // end FlinfoRaw

class FlinfoHTML extends FlinfoRaw {
	private $mLang;
	private $mDestFile;

	public function __construct ($params, $id, $userLang) {
		parent::__construct ($params, $id);
		$this->mLang     = $userLang ? $userLang : 'en'; // Fallback to English if not set set for whatever reason
		$this->mDestFile = isset ($params['destfile']) ? $params['destfile'] : null;
	}

	private function errMsg ($wiki, $displayInfo, $attributes, $errMsg) {
		$html = FlinfoHtmlGen::openElement ('div', array ('id' => 'flinfoError') + $attributes) . "\n";
		if ($wiki['status'] == FlinfoStatus::STATUS_BAD_LICENSE && $displayInfo['licenseStatus']) {
			$html .= FlinfoHtmlGen::text ($displayInfo['licenseStatus']);
			$html .= FlinfoHtmlGen::singleElement ('br');
		}
		$html .= FlinfoHtmlGen::text ($errMsg);
		$html .= FlinfoHtmlGen::closeElement ('div') . "\n";
		return $html;
	}

	public function format ($wiki, $raw, $displayInfo, $repositories) {
		header ('Content-Type: text/html; charset=utf-8');
		echo FlinfoHtmlGen::docType() . "\n";
		echo FlinfoHtmlGen::openElement ('html', array('xmlns' => 'http://www.w3.org/1999/xhtml')) . "\n";
		echo FlinfoHtmlGen::open ('head') . "\n";
		// Make Google ignore the search result page. It doesn't make sense to index this page.
		echo FlinfoHtmlGen::singleElement ('meta', array ('name' => 'robots', 'content' => 'noindex, nofollow')) . "\n";
		echo FlinfoHtmlGen::element ('title', null, FlinfoGlobals::$title) . "\n";
		echo FlinfoHtmlGen::close ('head') . "\n";
		echo FlinfoHtmlGen::open ('body') . "\n";
		echo FlinfoHtmlGen::openElement ('table', array ('border' => 0));
		echo FlinfoHtmlGen::open ('tr');
		echo FlinfoHtmlGen::open ('td') . "\n";
		echo FlinfoHtmlGen::element ('h1', array ('style' => 'margin-top:0;'), FlinfoGlobals::$title) . "\n";
		echo FlinfoHtmlGen::element ('h2', null, FlinfoData::msg('subtitle')) . "\n";
		echo FlinfoHtmlGen::open ('form') . "\n";
		echo FlinfoHtmlGen::openElement ('table', array ('border' => 0)) . "\n";
		echo FlinfoHtmlGen::open ('tr');
		echo FlinfoHtmlGen::open ('td') . "\n";
		echo FlinfoHtmlGen::element ('label', array ('for' => 'flickrId'), FlinfoData::msg('ID')) . "\n";
		echo FlinfoHtmlGen::newCell ();
		echo FlinfoHtmlGen::singleElement ('input', array ('type' => 'text', 'name' => 'id', 'id' => 'flickrId', 'size' => 80, 'value' => $this->mId)) . "\n";
		$repoTitle = null;
		if (is_array ($repositories) && count ($repositories) > 0) {
			if (count ($repositories) == 1) {
				echo FlinfoHtmlGen::singleElement ('input', array ('type' => 'hidden', 'name' => 'repo', 'value' => $repositories[0]['value'])) . "\n";
			} else {
				echo FlinfoHtmlGen::newRow ();
				echo FlinfoHtmlGen::element ('label', array ('for' => 'repo'), FlinfoData::msg('repo_label'));
				echo FlinfoHtmlGen::newCell () . "\n";
				echo FlinfoHtmlGen::openElement ('select', array ('name' => 'repo')) . "\n";
				foreach ($repositories as $r) {
					$attrs = array ('value' => $r['value'], 'title' => $r['title']);
					if (isset ($displayInfo['repo']) && $r['value'] == $displayInfo['repo']) {
						$attrs['selected'] = 'selected';
						$repoTitle = $r['text'];
					}

					echo '  ' . FlinfoHtmlGen::element ('option', $attrs, $r['text']) . "\n";
				}
				echo FlinfoHtmlGen::closeElement ('select') . "\n";
				echo "&nbsp;" . FlinfoHtmlGen::text (FlinfoData::msg('repo_hint')) . "\n";
			}
		}
		echo FlinfoHtmlGen::newRow () . "\n";
		echo FlinfoHtmlGen::singleElement ('input', array ('type' => 'checkbox', 'name' => 'raw'));
		echo FlinfoHtmlGen::text (FlinfoData::msg('raw')) . "\n";
		echo FlinfoHtmlGen::newCell ();
		echo FlinfoHtmlGen::singleElement ('input', array ('type' => 'submit', 'value' => FlinfoData::msg('get_button'))) . "\n";
		echo FlinfoHtmlGen::close ('td');
		echo FlinfoHtmlGen::close ('tr') . "\n";
		echo FlinfoHtmlGen::close ('table') . "\n";
		echo FlinfoHtmlGen::singleElement ('input', array ('type' => 'hidden', 'name' => 'user_lang', 'value' => $this->mLang)) . "\n";
		$extraItems = "";
		FlinfoHooks::run ('flinfoFormItems', array (&$extraItems));
		if (is_string ($extraItems) && $extraItems != "") {
			echo $extraItems;
		}
		echo "\n" . FlinfoHtmlGen::close ('form') . "\n";
		echo FlinfoHtmlGen::close('td') . "\n";
		echo FlinfoHtmlGen::openElement ('td', array ('align' => 'right', 'valign' => 'top')) . "\n";
		if ($displayInfo && $this->mId && isset ($displayInfo['sizes'])) {
			echo FlinfoHtmlGen::openElement ('div', array ('style' => 'font-size:small;'));
			if ($displayInfo['sizes'][0]['width'] > 0) {
				// Preview
				echo FlinfoHtmlGen::openElement ('div', array ('style' => 'float:right;margin-left:5px;'));
				echo FlinfoHtmlGen::openElement ('a', array ('href' => $displayInfo['desc'], 'title' => $displayInfo['title'], 'target' => '_blank'));
				$thumb = $displayInfo['sizes'][0];
				$w = $thumb['width'];
				$h = $thumb['height'];
				if ($w < 70 || $h < 70) {
					// Try to find a larger thumbnail
					foreach ($displayInfo['sizes'] as $thumbDesc) {
						if ($w > $thumbDesc['width'] || $h > $thumbDesc['height']) continue;
						if ($thumbDesc['height'] > 120) break;
						$thumb = $thumbDesc;
						if ($thumb['width'] >= 70 && $thumb['height'] >= 70) break;
					}
				}
				$w = $thumb['width'];
				$h = $thumb['height'];
				if ($h > 120) {
					$w = $w * 120 / $h; // Limit the preview size
					$h = 120;
				}
				echo FlinfoHtmlGen::singleElement ('img', array ('border' => 0, 'alt' => $displayInfo['title'], 'height' => $h, 'src' => $thumb['source']));
				echo FlinfoHtmlGen::close ('a') . "\n";
				echo FlinfoHtmlGen::close ('div') . "\n";
			}
			if ($repoTitle) {
				echo FlinfoHtmlGen::element (
						 'a'
						,array (
							 'href'   => $displayInfo['desc']
							,'title'  => $displayInfo['title']
							,'target' => '_blank'
						 )
						,str_replace ('_REPO_', $repoTitle, FlinfoData::msg('view_at'))
					 );
				echo FlinfoHtmlGen::singleElement('br');
			}
			$largestImage = $displayInfo['sizes'][count($displayInfo['sizes'])-1]['source'];
			echo FlinfoHtmlGen::element (
					 'a'
					,array (
						 'href'   => str_replace ('_HREF_', rawurlencode ($largestImage), FlinfoData::msg('view_exif_url'))
						,'title'  => FlinfoData::msg('view_exif_title')
						,'target' => '_blank'
					 )
					,FlinfoData::msg('view_exif')
				);
			echo FlinfoHtmlGen::close ('div') . "\n";
		}
		echo FlinfoHtmlGen::newRow (array ('colspan' => 2));
		if ($wiki !== null) {
			$stopEarly = true;
			FlinfoHooks::run ('flinfoStopEarly', array (&$stopEarly));
			$errMsg = $this->getErrorMsg($wiki['status'], $displayInfo['error']);
			if ($errMsg !== null && $stopEarly) {
				echo $this->errMsg($wiki, $displayInfo, array(), $errMsg);
			} else if ($this->mId) {
				// DestFile handling.
				$title = $this->mDestFile;
				if (!$title) $title = $displayInfo['uploadTitle'];
				if (!$title) $title = $displayInfo['title'];
				if ($title != "") {
					$title = preg_replace ('/\.jpe?g$/i', '', $title). '.jpg';
					// Try to make the title MediaWiki-safe.
					// See e.g. https://en.wikipedia.org/wiki/Wikipedia:Naming_conventions_%28technical_restrictions%29
					$title = preg_replace ('!~{3,}!', '', $title);
					$title = preg_replace ('!\s+!', ' ', $title);
					$title = preg_replace ('![\x00-\x1f\x7f]!', '', $title);
					$title = preg_replace ('!%([0-9A-Fa-f]{2})!', '% $1', $title);
					$title = preg_replace ('!&(([A-Za-z0-9\x80-\xff]+|#[0-9]+|#x[0-9A-Fa-f]+);)!', '& $1', $title);
					$title = preg_replace ('![:/|#]!', '-', $title);
					$title = preg_replace ('![\]\}>]!', ')', $title);
					$title = preg_replace ('![\[\{<]!', '(', $title);
				}
				// Although RFC 1738 does not mandate encoding of "'" even though it may be used to delimit entity attributes in HTML,
				// rawurlencode *does* actually encode it as %27. Note that we just assume that FlinfoData::msg('upload_form'] and
				// FlinfoData::msg('preview_form'] do not contain unencoded apostrophes...
				$uploadLink  = FlinfoData::msg('upload_form') . rawurlencode ($title);
				$previewLink = FlinfoData::msg('preview_form') . rawurlencode ($title);
				// Now create the preview/upload form
				echo FlinfoHtmlGen::openElement ('form', array ('id' => 'upload', 'method' => 'post', 'enctype' => 'multipart/form-data', 'action' => $previewLink, 'target' => '_blank')) . "\n";
				echo FlinfoHtmlGen::openElement ('table', array ('border' => '0'));
				echo FlinfoHtmlGen::open ('tr');
				echo FlinfoHtmlGen::open ('td') . "\n";
				$area = $this->createTextAreaContent($wiki);
				$nofLines = count (explode ("\n", $area)) + 1; // Trailing newline
				echo FlinfoHtmlGen::openElement ('textarea', array ('name' => 'wpInput', 'id' => 'wpInput', 'cols' => 100, 'rows' => $nofLines)) . "\n";
				echo FlinfoHtmlGen::text ($area);
				echo FlinfoHtmlGen::closeElement ('textarea') . "\n";
				if (isset ($displayInfo['alreadyFound']) && is_array($displayInfo['alreadyFound'])) {
					echo FlinfoHtmlGen::newRow(array('style' => 'background:yellow'));
					echo FlinfoHtmlGen::text(FlinfoData::msg('already_exists'));
					echo FlinfoHtmlGen::openElement('ul', array ('style' => 'margin-top:0;margin-bottom:0;list-style-type:none;')) . "\n";
					$idx = 0;
					foreach ($displayInfo['alreadyFound'] as $item) {
						echo FlinfoHtmlGen::open('li');
						if ($idx++ % 2 == 1) {
							echo FlinfoHtmlGen::openElement ('table', array ('border' => '0', 'width' => '100%', 'style' => 'background:#FFFF99;'));
						} else {
							echo FlinfoHtmlGen::openElement ('table', array ('border' => '0', 'width' => '100%'));
						}
						echo FlinfoHtmlGen::open ('tr');
						echo FlinfoHtmlGen::openElement ('td', array('width' => '100', 'valign' => 'middle', 'align' => 'center')) . "\n";
						if ($item['thumburl']) {
							echo FlinfoHtmlGen::openElement ('a', array ('href' => $item['url'], 'title' => $item['title'],'target' => '_blank'));
							if (isset($displayInfo['portrait']) && $displayInfo['portrait']) {
								echo FlinfoHtmlGen::singleElement('img', array ('border' => 0, 'alt' => $item['title'], 'src' => $item['thumburl'], 'height' => '66px'));
							} else {
								echo FlinfoHtmlGen::singleElement('img', array ('border' => 0, 'alt' => $item['title'], 'src' => $item['thumburl']));
							}
							echo FlinfoHtmlGen::close ('a');
						}
						echo FlinfoHtmlGen::close ('td');
						echo FlinfoHtmlGen::openElement ('td', array ('valign' => 'middle', 'align' => 'left')) . "\n";
						echo FlinfoHtmlGen::element (
								 'a'
								,array (
									 'href'   => $item['url']
									,'title'  => $item['title']
									,'target' => '_blank'
								 )
								,$item['title']
							 );
						echo FlinfoHtmlGen::close ('td');
						echo FlinfoHtmlGen::close ('tr');
						echo FlinfoHtmlGen::close ('table') . "\n";
						echo FlinfoHtmlGen::close('li') . "\n";
					}
					echo FlinfoHtmlGen::close('ul') . "\n";
				}
				$extraHtml = "";
				FlinfoHooks::run ('flinfoHtmlAfterTextArea', array (&$extraHtml));
				if (!is_string ($extraHtml)) $extraHtml = "";
				if ($errMsg !== null) {
					$extraHtml .= $this->errMsg($wiki, $displayInfo, array ('style' => 'background:pink;'), $errMsg);
				}
				if ($extraHtml != "") {
					echo FlinfoHtmlGen::newRow ();
					echo FlinfoHtmlGen::openElement ('div', array ('id' => 'flinfoExtraStuff', 'style' => 'margin-top:0.5em;width:100ex;'));
					echo $extraHtml;
					echo FlinfoHtmlGen::close ('div') . "\n";
					// Fix the width of that div. It appears there is no way in HTML or in CSS to specify
					// that one particular cell shall not be considered when computing the width of a column,
					// or to limit the width of this div to that of the textarea. So we set it initially to
					// some halfway reasonable value (100ex should be about 100 columns, maybe a bit less), and
					// then fix it in JavaScript.
					echo FlinfoHtmlGen::openInlineScript ();
					echo "(function () {\n";
					echo "  var textbox = document.getElementById ('wpInput');\n";
					echo "  var field = document.getElementById ('flinfoExtraStuff');\n";
					echo "  if (!textbox || !field) return;\n";
					echo "  var w = textbox.offsetWidth;\n";
					echo "  if (!w || w <= 0) return;\n";
					echo "  field.style.width = '' + w + 'px';\n";
					echo "})();\n";
					echo FlinfoHtmlGen::closeInlineScript () . "\n";
				}
				// We use POST requests through form submission for both the preview *and* the upload form, to be able to handle
				// large descriptions. (The WMF servers, like any standard Apache config, refuse to handle GET requests larger
				// than 4kB. Additionally, IE has a limit of about 2kB on the length of a URL.) We can use the same form for both
				// requests because we dynamically change, through JavaScript, the form action and the name of the textbox.
				echo FlinfoHtmlGen::newRow (array ('style' => 'padding-top:0.5em;'));
				echo FlinfoHtmlGen::singleElement ('input', array ('type' => 'submit', 'name' => 'wpPreview', 'value' => FlinfoData::msg('preview'), 'onclick' => 'open_preview()')) . "\n";
				echo FlinfoHtmlGen::singleElement ('input', array ('type' => 'button', 'name' => 'wpUpload', 'id' => 'wpUpload', 'value' => FlinfoData::msg('open_upload_form'), 'onclick' => 'open_upload()', 'style' => 'display:none;')) . "\n";
				echo FlinfoHtmlGen::openInlineScript ();
				echo "(function(){var b = document.getElementById ('wpUpload'); if (b) b.style.display = '';})();\n";
				echo "function open_upload() {\n";
				echo "  var form = document.getElementById ('upload');\n";
				echo "  form.action = '" . $uploadLink . "';\n";
				echo "  var textbox = document.getElementById ('wpInput');\n";
				echo "  textbox.name = 'wpUploadDescription';\n";
				echo "  form.submit();\n";
				echo "}\n";
				echo "function open_preview() {\n";
				echo "  var form = document.getElementById ('upload');\n";
				echo "  form.action = '" . $previewLink . "';\n";
				echo "  var textbox = document.getElementById ('wpInput');\n";
				echo "  textbox.name = 'wpInput';\n";
				echo "  form.submit();\n";
				echo "}\n";
				echo FlinfoHtmlGen::closeInlineScript () . "\n";
				if (isset ($displayInfo['sizes'])) {
					$last = count($displayInfo['sizes'])-1;
					echo FlinfoHtmlGen::text ('[');
					echo FlinfoHtmlGen::element ('a', array ('href' => $displayInfo['sizes'][$last]['source'], 'target' => '_blank'), FlinfoData::msg('download_org'));
					echo FlinfoHtmlGen::text (']');
					if ($displayInfo['sizes'][$last]['width'] > 0 && $displayInfo['sizes'][$last]['height'] > 0) {
						echo FlinfoHtmlGen::text (' (' . $displayInfo['sizes'][$last]['width']);
						echo "&times;";
						echo FlinfoHtmlGen::text ($displayInfo['sizes'][$last]['height'] . ' ' . FlinfoData::msg('pixels') . ')') . "\n";
					}
				}
				echo FlinfoHtmlGen::close ('form') . "\n";
				echo FlinfoHtmlGen::close ('td');
				echo FlinfoHtmlGen::close ('tr') . "\n";
				echo FlinfoHtmlGen::close ('table') . "\n";
			}
		}
		echo FlinfoHtmlGen::close ('td');
		echo FlinfoHtmlGen::close ('tr') . "\n";
		echo FlinfoHtmlGen::close ('table') . "\n";
		// Footer
		echo FlinfoHtmlGen::singleElement ('hr') . "\n";
		echo FlinfoHtmlGen::text ('Flinfo ' . FlinfoGlobals::VERSION) . " &nbsp; ";
		echo FlinfoHtmlGen::element ('a', array ('href' => FlinfoData::msg('manual_link'), 'target' => '_blank'), FlinfoData::msg('manual'));
		echo ' &ndash; ';
		echo FlinfoHtmlGen::element ('a', array ('href' => FlinfoData::msg('contact_link'), 'target' => '_blank'), FlinfoData::msg('contact'));
		echo ' &ndash; by ';
		echo FlinfoHtmlGen::element ('a', array ('href' => 'https://de.wikipedia.org/wiki/Benutzer:Flominator', 'target' => '_blank'), 'Flominator');
		echo FlinfoHtmlGen::text (' & ');
		echo FlinfoHtmlGen::element ('a', array ('href' => 'https://commons.wikimedia.org/wiki/User:Lupo', 'target' => '_blank'), 'Lupo');
		echo ' &ndash; ';
		echo FlinfoHtmlGen::element ('a', array ('href' => FlinfoData::msg('source_link'), 'target' => '_blank'), FlinfoData::msg('source')) . "\n";
		$extraHtml = "";
		FlinfoHooks::run ('flinfoEndOfHtml', array (&$extraHtml));
		if (is_string ($extraHtml) && $extraHtml != "") {
			echo FlinfoHtmlGen::singleElement('br');
			echo $extraHtml;
		}
		echo FlinfoHtmlGen::close('body') . "\n";
		echo FlinfoHtmlGen::close('html') . "\n";
	}

} // end FlinfoHTML