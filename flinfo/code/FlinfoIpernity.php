<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoStatus.php');
require_once ('FlinfoIn.php');

require_once ('api/FlinfoIpernityAPI.php');

/**
 * Input handler for ipernity.
 */
class FlinfoIpernity extends FlinfoIn {

	private $mServer = null;
	private $mInfo   = null;
	private $mId     = null;
	private $mUserId = null;
	private $mUrl    = null;

	public function __construct ($parameterFileName, $requestParams) {
		$params = $this->getParamFile ($parameterFileName);
		if ($params) {
			$lines = explode ("\n", $params);
			$apiKey = trim ($lines[0]);
			$secret = trim ($lines[1]);
			$this->mServer = new FlinfoIpernityAPI($apiKey, $secret);
		}
	}

	private function extractId ($rawId) {
	    if (preg_match ('/^\d+$/', $rawId)) {
			// All digits: assume a photo id.
			return array ('id' => $rawId, 'uid' => null);
	    } else if (preg_match ('!^https?://(www\.)?ipernity\.com/doc/([^/]+)/(\d{4,})(/.*)?$!', $rawId, $matches)) {
	    	return array ('id' => $matches[3], 'uid' => $matches[2]);
	    } else if (preg_match ('!^https?://([^.]+\.)?ipernity\.com/\d+/(\d\d)/(\d\d)/(\d{4,})\..*$!', $rawId, $matches)) {
	    	$d = $matches[2] . $matches[3];
	    	$id = $matches[4];
	    	$l = strlen ($id);
	    	if (substr ($id, $l - 4) == $d) {
	    		return array ('id' => $id, 'uid' => null);
	    	} else {
	    		return null;
	    	}
		} else {
			// Id could not be determined
			return null;
		}
	}

	public function getInfo ($id) {
		if (!$this->mServer) {
			return array ($id, FlinfoStatus::STATUS_INTERNAL_ERROR);
		}
		$this->mIdDesc = $this->extractId ($id);
		if (!$this->mIdDesc) {
			return array ($id, FlinfoStatus::STATUS_INVALID_ID);
		}
		$this->mId = $this->mIdDesc['id'];
		$data = $this->mServer->getInfo ($this->mId, 'tags,geo');
		if (is_string ($data)) {
			$this->setServerError ($data);
			return array ($this->mId, FlinfoStatus::STATUS_SERVER_FAILURE);
		}
		$this->mInfo = $data['doc'];
		$this->mUserId = $this->mInfo['owner']['user_id'];
		if (   $this->mInfo['doc_id'] != $this->mId
		    ||    $this->mIdDesc['uid'] != null
		       && $this->mIdDesc['uid'] != $this->mUserId
		       && $this->mIdDesc['uid'] != $this->mInfo['owner']['alias']
		   )
		{
			// Guard against invalid URL inputs returning info about some other image.
			return array ($this->mId, FlinfoStatus::STATUS_INVALID_ID);
		}
		$this->mUrl = $this->mInfo['link'];
		// Sort the thumbs, and append the original from $this->mInfo['original'].
		if (isset($this->mInfo['thumbs']) && isset($this->mInfo['thumbs']['thumb']) && is_array($this->mInfo['thumbs']['thumb'])) {
			usort($this->mInfo['thumbs']['thumb'], array($this, 'cmpThumbs'));
			if (isset($this->mInfo['original'])) {
				$this->mInfo['thumbs']['thumb'][] = $this->mInfo['original'];
			}
		}
		return array ($this->mId, FlinfoStatus::STATUS_OK);
	}

	private function cmpThumbs ($a, $b) {
		// Sorts by number of pixels
		return intval($a['w']) * intval($a['h']) - intval($b['w']) * intval($b['h']);
	}

	public function getAccountId () {
		return $this->mUserId;
	}

	/*
	 * No, we cannot get that from Ipernity. They don't have an API for it...
	 * Definitions are from http://www.ipernity.com/help/api/method/doc.setLicense
	 * It's unclear to me what "copyleft" means. GFDL? FAL? GPL?
	 */
	static $ipernityLicenses = array (
        '0' => "All rights reserved"
     ,  '1' => "Attribution (cc-by)"
     ,  '3' => "Attribution-NonCommercial (cc-by-nc)"
     ,  '5' => "Attribution-NoDerivs (cc-by-nd)"
     ,  '7' => "Attribution-NonCommercial-NoDerivs (cc-by-nc-nd)"
     ,  '9' => "Attribution-ShareAlike (cc-by-sa)"
     , '11' => "Attribution-NonCommercial-ShareAlike (cc-by-nc-sa)"
     ,'255' => "Copyleft"
	);

	private function getLicenseName ($index)
	{
		if (isset(self::$ipernityLicenses[$index])) {
			return self::$ipernityLicenses[$index];
		}
		return "UNKNOWN IPERNITY LICENSE";
	}

	public function getLicenses ($goodUser)	{
		$tags = array();
		$source = null;
		$license = $this->mInfo['license'];
		$status = 0;
		if ($goodUser) {
			switch ($license) {
				case '1':
					$status = 1;
					$tags[] = 'cc-by-3.0';
					break;
				case '9':
					$status = 9;
					$tags[] = 'cc-by-sa-3.0';
					break;
				default:
					$status = 0;
			}
		}
		if ($status == 0) {
			$status = $this->getLicenseName ($license);
		} else {
			$status = null;
			$tags[] = $this->getReviewTag();
		}
		return array ($status, $tags, $source);
	}

	protected function internalGetReviewTag () {
		return "ipernityreview";
	}

	public function getAuthor () {
		$userName = $this->mInfo['owner']['username'];
		if (!$userName || $userName == "") $userName = $this->mInfo['owner']['alias'];
		if (preg_match ('/^\d+$/', $userName)) {
			// Only digits: it's an Id
			$userName = null;
		}
		if (!$userName || $userName == "") {
			$userName = "An ipernity user";
		}
		$authorUrl = 'http://www.ipernity.com/';
		if ($this->mUserId) $authorUrl .= 'doc/' . $this->mUserId;
		return array (array ($authorUrl, $userName, null));
	}

	public function getDate () {
		return $this->convertISODate($this->mInfo['dates']['created']);
	}

	public function getSource () {
		if (!$this->mUrl) $this->mUrl = $this->getAlternateSource();
		$title = $this->getTitle();
		if (!$title || $title == "") $title = 'ipernity';
		return array ($this->mUrl, $title);
	}

	public function getAlternateSource() {
		return 'http://www.ipernity.com/doc/' . $this->mUserId . '/' . $this->mId;
	}

	public function getRawResult () {
		return $this->mInfo;
	}

	public function getDescription () {
		if ($this->mInfo) {
			return $this->mInfo['description'];
		}
		return "";
	}

	public function getGeoInfo () {
		if (   $this->mInfo
		    && isset ($this->mInfo['geo'])
		    && isset ($this->mInfo['geo']['lat'])
		    && isset ($this->mInfo['geo']['lng'])
		    && isset ($this->mInfo['geo']['loc_id']) // See ipernity ticket #8309
		   )
		{
			return array ( 'latitude' => $this->mInfo['geo']['lat']
			              ,'longitude' => $this->mInfo['geo']['lng']
			              ,'source' => 'ipernity');
		}
		return null;
	}

	public function getTitle () {
		if ($this->mInfo) {
			return $this->mInfo['title'];
		}
		return "";
	}

	public function getSizes () {
		if (!$this->mInfo || !isset ($this->mInfo['thumbs']) || !isset ($this->mInfo['thumbs']['thumb'])) {
			return null;
		}
		$result = array ();
		$sizes = $this->mInfo['thumbs']['thumb'];
		// Format into *our* format: 'width', 'height', 'source'
		foreach ($sizes as $thumb) {
			$result[] = array ('width' => $thumb['w'], 'height' => $thumb['h'], 'source' => $thumb['url']);
		}
		return $result;
	}

	public function getCategories () {
		$result = array ();
		if ($this->mInfo && isset ($this->mInfo['tags']) && isset ($this->mInfo['tags']['tag'])) {
			foreach ($this->mInfo['tags']['tag'] as $tag) {
				$result[] = strtoupper (substr($tag['tag'], 0, 1)) . substr($tag['tag'], 1);
			}
		}
		return $result;
	}

}