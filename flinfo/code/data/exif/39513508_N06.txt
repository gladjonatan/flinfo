ImageDescription,Caption-Abstract = #U\.?S\.?\s+Air\s+[Ff]orce\s+[Pp]hoto# > PD-USGov-Military-Air Force
Copyright,CopyrightNotice,Rights = #^Public\s+Domain$#i > PD-USGov-Military-Air Force
Instructions,SpecialInstructions = #[Rr]eleased\s+by\s.*AFCENT# > PD-USGov-Military-Air Force
OriginalTransmissionReference = #^AFCENT$# > PD-USGov-Military-Air Force
#Official U.S. Air Force