<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Flinfo
 *
 * Query image repositories about photos, extract information in a suitable format for use on
 * Wikimedia Commons.
 *
 * Written by
 * - Florian Straub  (flominator@gmx,net)
 *   - Original version 1.0
 * - Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *   - Version 2.0 and onward
 */

// Avoid errors/warnings on time functions.
if (function_exists ('date_default_timezone_set')) date_default_timezone_set ('UTC'); // PHP >= 5.1.0;

// Add our own directory to the include path so we don't always have to repeat the pathes in our requires
// First figure out which separator to use.
if (strpos(__FILE__, ':') !== false) {
	$path_delimiter = ';';
} else {
	$path_delimiter = ':';
}
// Then adjust the path accordingly.
ini_set('include_path', ini_get('include_path') . $path_delimiter . dirname(__FILE__));

require_once ('FlinfoData.php');

// Before doing *anything* else, make sure this dreadful "magic quote" feature is turned off!
FlinfoData::turnOffMagicQuoting();

require_once ('FlinfoExtensions.php');
require_once ('FlinfoGlobals.php');
require_once ('FlinfoOut.php');
require_once ('FlinfoWiki.php');

function flinfo ($titleSuffix = null)
{
	if ($titleSuffix && is_string ($titleSuffix)) {
		FlinfoGlobals::$title .= $titleSuffix;
	}
	if (isset ($_REQUEST['format'])) {
		$format = $_REQUEST['format'];
	} else {
		$format = 'html';
		if (isset ($_REQUEST['raw']) && $_REQUEST['raw'] == 'on') {
			$format = 'raw';
		}
	}

	// Get server info and transform into wiki object
	$inputHandler = new FlinfoWiki($_REQUEST, $format == 'html', $format == 'html' || $format == 'raw');
	$wiki = $inputHandler->generate();
	$id   = $inputHandler->getId();
	$displayInfo = $inputHandler->getDisplayInfo();
	$facades = $inputHandler->getFacadeNames();

	// Create an output handler
	$formatter = FlinfoOut::create($format, $_REQUEST, $id);

	// Generate output. Note that this will set the response headers, so be careful that you
	// have not generated any output before!
	$formatter->format($wiki, $inputHandler->getRawServerResult(), $displayInfo, $facades);

}
