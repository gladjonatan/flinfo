<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Some global constants.
 */
abstract class FlinfoGlobals {

	/**
	 * Flinfo version
	 * @var string
	 */
	const VERSION = '2.7';

	/**
	 * User-agent header to be used in remote requests made by Flinfo
	 * @var string
	 */
	const USER_AGENT = 'Flinfo by de_user_Flominator and commons_user_Lupo';

	/**
	 * The name of this program. Not const because the main entry point may
	 * want to change it.
	 * @var string
	 */
	public static $title = 'Flinfo';

}
