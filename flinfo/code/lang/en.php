<?php

//Form elements
$messages['subtitle']= 'The easy way to upload to Wikimedia Commons';
$messages['ID'] = 'Photo ID or URL:';
$messages['raw'] = 'Raw output';
$messages['get_button'] = 'Get Info';

$messages['preview'] = 'Preview';
$messages['upload_form'] = 'https://commons.wikimedia.org/w/index.php?title=Special:Upload&uselang=en&uploadformstyle=basic&wpDestFile=';
$messages['preview_form'] = 'https://commons.wikimedia.org/w/index.php?title=Special:ExpandTemplates&uselang=en&wpRemoveComments=1&wpContextTitle=';
$messages['download_org'] = 'Download largest available size';
$messages['open_upload_form'] = 'Open upload form';
$messages['pixels'] = 'pixels';
$messages['repo_label'] = 'Image repository:';
$messages['repo_hint'] = '(ignored if a URL that uniquely identifies the repository is entered)';

$messages['view_at'] = 'View at _REPO_';
$messages['view_exif'] = 'View EXIF';
$messages['view_exif_url'] = 'http://regex.info/exif.cgi?url=_HREF_';
$messages['view_exif_title'] = "using Jeffrey's Exif Viewer";

//Textarea
$messages['created'] = "%Y-%m-%d %H:%M";
$messages['copyvio_template'] = 'copyvio';
$messages['speedy_template'] = 'speedy|reason=Photo from a [[:commons:User:FlickreviewR/bad-authors|blacklisted]] Flickr user. --~~~~';
$messages['copyvio_reason'] = 'Licensed at the source as "_LICENSE_NAME_", which is not a free license.';
$messages['new_commons'] = "Found an unknown Flickr-Commons participant. [[User:Flominator]] has been notified. Please re-try in a few days.";
$messages['uncat'] = '{{subst:unc}}';

//Footer
$messages['manual'] ='Help';
$messages['manual_link'] ='https://commons.wikimedia.org/wiki/User:Flominator/Flinfo';
$messages['contact']='Contact';
$messages['contact_link']='https://commons.wikimedia.org/w/index.php?title=User_talk:Flominator/Flinfo';
$messages['source']='Source';
$messages['source_link']='https://gitlab.com/flinfo/flinfo';

//Errors & Warnings
$messages['error_nonfree'] = 'Picture is not freely licensed; try another one.';
$messages['error_server']='Problem while retrieving image _PHOTOID_. _FLINFO_SERVER_ERROR_';
$messages['error_internal']='Internal error.';
$messages['error_baduser']="Blacklisted user; images from that user's pages must not be uploaded to Wikimedia Commons.";
$messages['error_invalid_id']='Cannot determine image ID from input _PHOTOID_';
$messages['error_missing_id']='Image ID required';

$messages['license_override']='_PROGRAM_ has chosen a license from the image description or its metadata that is less restrictive than the "_LICENSE_" license displayed at _REPO_.';
$messages['download_original_hint'] = 'Please make sure that you download and then upload to Wikimedia Commons the original size of the image (download link below).';
$messages['missing_species']='No Commons category for "_SPECIES_" found. Please check how to categorize this file at the Commons.';
$messages['license_override_tag']='_PROGRAM_ has overridden the license displayed at _REPO_ ("_LICENSE_") because field "_FIELD_" was "_VAL_".';

$messages['already_exists']='This image possibly exists already at the Commons:';
