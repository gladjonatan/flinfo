<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoData.php');

/**
 * Simple support for file-cached queries.
 */
class FlinfoCache {
	
	private $mFileName;
	private $mCallback;
	private $mCachePeriod;
	
	/**
	 * Create a new FlinfoCache, using the given filename to cache the result of the callback for the given period.
	 *
	 * @param string $fileName File name of the cache file
	 * @param mixed  $loadCallback Callback to call to refresh the cached contents. See {@link call_user_func_array}.
	 *                             Is called without arguments and should return null upon failure or a string
	 *                             defining the contents otherwise.
	 * @param int    $cachePeriod  Time in seconds for which the contents should be cached.
	 * @return FlinfoCache
	 */
	public function __construct ($fileName, $loadCallback, $cachePeriod) {
		$this->mFileName = FlinfoData::dataDirectory() . '/' . $fileName;
		$this->mCallback = $loadCallback;
		$this->mCachePeriod = $cachePeriod;
	}

    /**
     * Get the cached data. If $forceRefresh = true, or the cache period has expired, the cache is refreshed before
     * returning the contents.
     *
     * @param boolean $forceRefresh If true, force a cache refresh.
     * @return string The cached data; null if none available.
     */
	public function get($forceRefresh = false) {
		if ($forceRefresh || !file_exists ($this->mFileName) || time () - filemtime ($this->mFileName) > $this->mCachePeriod) {
			// Doesn't exist, or is older than the given cache period
			$data = call_user_func_array ($this->mCallback, array());
			if ($data) {
				file_put_contents ($this->mFileName, $data, LOCK_EX);
			} else if (is_file ($this->mFileName)) {
				// Just use old cached contents
				$data = file_get_contents ($this->mFileName);
			}
			return $data;
		} else {
			return file_get_contents ($this->mFileName);
		}
	}	
}