<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('lib/Curly.php');
require_once ('lib/FormatJson.php');

/**
 * Basic Picasa API.
 * 
 * Only implemented function is what's interesting for flinfo: getting
 * image and album information including tags and geolocation.
 */
class FlinfoPicasaAPI {
	
	public function __construct () {
	}

	private function request ($url) {		
		$data = Curly::singleRequest (Curly::getRequest ($url), $errorMsg);
		if ($errorMsg !== null) {
			if (is_string ($data)) $errorMsg .= ': ' . $data;
			return $errorMsg;
		}
		return self::parse ($data);		
	}

	/**
	 * Get basic photo info, including author, license, description, timestamp, geolocation, EXIF data, and album ID.
	 * 
	 * @param int $imageId  Picasa photo ID
	 * @param string $user  User name
	 * @return mixed array, if successful, containing the parsed Picasa response; or a string with an error message otherwise.
	 */
	public function getPhotoInfo ($imageId, $user, $auth=null) {
		$url = 'http://picasaweb.google.com/data/feed/tiny/user/' . $user;
		$url .= '/photoid/' . $imageId;
		$url .= '?alt=json&urlredir=1&max-results=1';
		if ($auth) $url .= '&' . $auth;
		return $this->request($url);
	}
	
	/**
	 * Get all thumbnail and scaled image links, including a link to the original photo.
	 * 
	 * @param int $imageId Picasa photo ID
	 * @param string $user User name
	 * @return mixed array, if successful, containing the parsed Picasa response; or a string with an error message otherwise.
	 */
	public function getThumbnails ($imageId, $user, $auth=null) {
		$url = 'http://picasaweb.google.com/data/feed/api/user/' . $user;
		$url .= '/photoid/' . $imageId;
		// All allowed thumnail sizes as per Google's API docs. The "c" values are square crops of the original, the "u"
		// values are simply scaled.
		$url .= '?alt=json&urlredir=1&max-results=1&thumbsize=32c,48c,64c,72c,104c,144c,150c,160c,32u,48u,64u,72u,104u,144u,150u,160u,94,110,128,200,220,288,320,400,512,576,640,720,800,912,1024,1152,1280,1440,1600&imgmax=d&fields=media:group/media:content,media:group/media:thumbnail';
		if ($auth) $url .= '&' . $auth;
		// Note: there is one additional URL available, with "/s0": that is a link to the original size. However, we'll get an
		// "official" link to the original size, with a "/d", in media$group:media$content. In a browser, these two links
		// behave differently: "/d" makes Firefox open a save dialog, whereas "/s0" simply displays the image in the browser.
		// Also, the /s0 version sometimes has (some of) the EXIF stripped, while the /d version retains it.
		return $this->request($url);
	}
	
	/**
	 * Get information about a Picasa album.
	 * 
	 * @param mixed $albumId int or string: album ID or album name
	 * @param unknown_type $user User name
	 * @return mixed array, if successful, containing the parsed Picasa response; or a string with an error message otherwise.
	 */
	public function getAlbumInfo ($albumId, $user, $auth=null) {
		// Albums are interesting for us because they allow us to construct saner links and because they also can have
		// a geolocation.
		$url = 'http://picasaweb.google.com/data/feed/tiny/user/' . $user;
		if (preg_match ('/\D/', $albumId)) {
			$url .= '/album/' . $albumId;
		} else {
			$url .= '/albumid/' . $albumId;
		}
		$url .= '?alt=json&urlredir=1&max-results=1';
		if ($auth) $url .= '&' . $auth;
		return $this->request($url);
	}
	
	/**
	 * Remove all the spurious "$t" nodes, hoisting them up.
	 * 
	 * @param mixed $a Picasa reponse, mixed array-object hierarchy
	 * @return array Cleaned Picasa response, with all objects converted to arrays and "$t" nodes removed.
	 */
	private static function clean ($a) {
		$arr = null;
		switch (gettype ($a)) {
			case 'array' :
				if (count ($a) == 0) return $a;
				$arr = $a;
				break;
			case 'object' :
				$arr = get_object_vars ($a);
				if (count ($arr) == 0) return array();
				break;
			default:
				return $a;
		}
		if ($arr === null) return $a; // Paranoia
		if (count($arr) == 1 && array_key_exists('$t', $arr)) return $arr['$t'];
		if (count($arr) == 2
		    && array_key_exists('$t', $arr) && is_string ($arr['$t'])
		    && array_key_exists('type', $arr) && ($arr['type'] == 'text' || $arr['type'] == 'plain')
		   ) {
		    return $arr['$t'];
		}
		// Converts object to arrays, too
		foreach ($arr as $k => $v) {
			$arr[$k] = self::clean($v);
		}
		return $arr;
	}

	/**
	 * Parse a Picasa response into a php data structure.
	 * 
	 * @param string $data Picasa JSON response string
	 * @return array Parsed Picasa JSON response as nested php arrays
	 */
	public static function parse ($data) {
		if (!$data) return null;
		return self::clean (FormatJson::decode ($data, true));
	}
		
}
