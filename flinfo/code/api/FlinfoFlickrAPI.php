<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('lib/Curly.php');

/**
 * Simple Flickr API. Only stuff interesting for Flinfo is supported. No authorization,
 * no uploads. Much smaller than phpFlickr, and does not need a session.
 */
class FlinfoFlickrAPI {
	
	private $apiKey = null;
	private $secret = null;
	
	public function __construct ($apiKey, $secret = null) {
		$this->apiKey = $apiKey;
		$this->secret = $secret;
	}
	
	/**
	 * Execute a Flickr request for the given function.
	 * @param String $functionName Name of the Flickr function, e.g. "flickr.photos.getInfo"
	 * @param array $params Array containing the parameters.
	 * @return unknown_type (String or array)
	 *   If a string is returned, it's an error message. Otherwise, returns the server response as an array.
	 */
	private function request ($functionName, $params) {
		$params['api_key'] = $this->apiKey;
		$params['format'] = 'php_serial';
		$params['method'] = $functionName;
		if ($this->secret) {
			$params['api_sig'] = $this->signParameters ($params);
		}
		$url = 'https://api.flickr.com/services/rest/';
		
		$req = Curly::postRequest ($url, $params);
		$data = Curly::singleRequest ($req, $errorMsg);
    	if ($errorMsg !== null) {
    		return $errorMsg;
    	}
	    $result = $this->clean (unserialize ($data));
	    if (is_array ($result) && isset ($result['stat'])) {
	    	if ($result['stat'] != 'ok') {
	    		return $result['message'];
	    	}
	    	return $result;
	    }
		return 'Unknown format of server response.';
	}

	private function clean ($arr) {
		if (!is_array($arr) || count($arr) == 0) return $arr;
		if (count($arr) == 1 && array_key_exists('_content', $arr)) {
			return $arr['_content'];
		} else {
			foreach ($arr as $key => $element) {
				$arr[$key] = $this->clean ($element);
			}
			return $arr;
		}
	}
	
	private function signParameters ($params) {
		ksort ($params);
		$signature = '';
		foreach ($params as $k => $v) {
			$signature .= $k . $v;
		}
		return md5($this->secret . $signature);
	}
	
    public function photos_getInfo ($photoID) {
    	return $this->request ('flickr.photos.getInfo', array('photo_id' => $photoID));
    }
    
    public function photos_getSizes ($photoID) {
		$data = $this->request('flickr.photos.getSizes', array('photo_id' => $photoID));
		if (is_string ($data)) return $data;
		if (isset($data['sizes']) && isset($data['sizes']['size'])) {
			return $data['sizes']['size'];
		}
		return false;
    }
    
    public function photos_getExif ($photoID) {
		$data = $this->request('flickr.photos.getExif', array('photo_id' => $photoID));
		if (is_string ($data)) return $data;
		if (isset($data['photo']) && isset($data['photo']['exif'])) {
			return $data['photo']['exif'];
		}
		return false;
    }
    
    public function commons_getInstitutions () {
		$data = $this->request('flickr.commons.getInstitutions', array());
		if (is_string ($data)) return $data;
		if (isset($data['institutions']) && isset($data['institutions']['institution'])) {
			return $data['institutions']['institution'];
		}
		return false;
	}
	
	public function photos_licenses_getInfo () {		
		$data = $this->request('flickr.photos.licenses.getInfo', array());
		if (is_string ($data)) return $data;
		if (isset($data['licenses']) && isset($data['licenses']['license'])) {
			return $data['licenses']['license'];
		}
		return false;
	}

	
}
    	